Network Protocol
================
The Gate88 network protocol is a simple protocol that is used for communication between game clients and servers. The protocol implements a reliability layer on top of UDP and sends all data in big endian format.

Command Structure
-----------------
Each `Command` consists of a 3 byte header followed by the payload of the command. The header consists of a `type` field which identifies the `Command` being sent and a `length` field which indicates the length of the `payload`. However, the code doesn't use the `length` field to determine how much data after the header to consume. Rather, it compares the length to a stored value and discards the rest of the UDP datagram if the values do not match.

       1     2     ...
    [type|length|payload]

Certain important `Command`s are marked as reliable. For these `Command`s, the value of `length` is the size of the `payload` less two bytes. These two bytes represent the `g_counter` and is used to facilitate a reliable UDP protocol.

       1     2       2       ...
    [type|length|g_counter|payload]

Reliable UDP protocol
---------------------
Whenever a reliable `Command` is received, the receiver must acknowledge the `Command` by responding with a `CommandAcknowledge` with the same `g_counter` field as the one it received. If the sender does not receive a `CommandAcknowledge` after a set interval of time, it resends the `Command`. Note that `Command`s never expire unless if the receiver times out; a sender will keep reattempting to send a packet forever until it receives the correct `CommandAcknowledge` response.