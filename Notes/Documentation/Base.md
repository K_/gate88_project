Base.pm
=======
The parent class for all Gate88 clients/servers. The following functionality is offered:

- Network management functionality via an instance of `NetworkManager`*
- Timer management functionality via an instance of `TimerManager`*
- Command registration functionality

*Convenience methods are offered for both in the form of `addcmdcallback` ande `addintvcallback`.

new
---
The constructor accepts a IO::Socket::INET object so as to allow the subclass to specify whether it wishes to listen or connect to a server.

run
---
The main logic loop. Runs every 100ms.

proc
----
Processes 

addcommand
----------
Register a new command with the class.

addcmdcommand
-------------
Register a new callback with `NetworkManager`. The callback will be passed a reference to the current `Base` object.

addcmdcommand
-------------
Register a new callback with `TimerManager`. The callback will be passed a reference to the current `Base` object.

addtimeoutcallback
-------------
Register a new timeout callback with `NetworkManager`. The callback will be passed a reference to the current `Base` object.