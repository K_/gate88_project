Bot_Base
========
The base class for any state machine based bots.

new
---
Accepts a name to connect to the server with.

proc_state
----------
Processes the current state if the `StateStack` is not empty.