Bot
===
The base class for any bots. All the nitty-gritty details of connecting to a server and maintaining the connection are handled in this class.

new
---
Accepts a name to connect to the server with.

registercallbacks
-----------------
Registers all the callbacks required 

connect
-------
Connects to the server.

timeout
-------
Reconnects to the server if a disconnect occurs.

shutdown
--------

command_quit
------------
Calls shutdown before quitting the bot.

command_talk
------------
Sends a chat message to the server.

sendcmdserver
-------------
Send a command to the server. Convenience method which wraps `NetworkManager::sendcmd`.

queuecmdserver
-------------
Queue a command to the server. Convenience method which wraps `NetworkManager::queuecmd`.

getplayer
---------
Return the player object associated with a given id.

getplayername
---------
Return the player name associated with a given id.

build
-----
Send a command to the server indicating a build event.