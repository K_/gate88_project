State
=====
Represents a state of the Bot. Each method (with the exception of `new` is passed an instance of the `StateStack`, the current `GameState` and the `NetworkManager`.

new
---
Any variables should be declared here.

init
----
Called once when the `State` is added to the `StateStack`. If necessary, define any variables.

enter
-----
Called whenever this `State` is entered.

proc
----
Called every tick when this is the active `State`. Return* `false` to pop off the `State` and `true` to do nothing.

*If the stack is manipulated prior to returning, the _top_ `State` will be removed, not the current one.

event
-----
Called when an event which this `State` is interested in is received.

leave
-----
Called whenever this `State` is left.

destroy
-------
Called once when this `State` is removed from the `StateStack`.