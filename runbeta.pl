#!/usr/bin/perl
use Modern::Perl;

use Gate88::Bot::Beta;

my $bot;

$SIG{'INT'} = sub {
    $bot->shutdown();
    exit;
};

$bot = Gate88::Bot::Beta->new();
$bot->run();