#!/usr/bin/perl
use Modern::Perl;

use Gate88::Bot;

my $bot;

$SIG{'INT'} = sub {
    $bot->shutdown();
    exit;
};

$bot = Gate88::Bot->new('[Bot] CrashMe');
$bot->run();