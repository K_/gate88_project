use strict;
use warnings;

use lib '..';
use Switch;
use OpenGL qw(:all);
use Math::Trig;

use Gate88::Bot;
use Gate88::Data::Entity;

#DBG
my $ind=0;
my @spawn=(3,4, 5,7,8,10,11,14,15,20,21);
my $bot=Gate88::Bot->new();
#//

#my @winsize=(400,600);
my @winsize=(600,400);
my @viewport=(0,0);
my @pos=(0, 0);
my @mouse=(0, 0);
my @keys=();
my @entcolor=(
	[0, 53/256, 0], 
	[0, 50/256, 130/256], 
	[107/256, 0, 1/256]
);
my $szdiv=600;
my $scale=1;

glutInit();

glutDisplayFunc(\&run);
glutIdleFunc(\&run);

glutInitWindowSize($winsize[0], $winsize[1]);
my $win=glutCreateWindow('Game View');

resize(@winsize);
glutReshapeFunc(\&resize);
glutCloseFunc(sub {print "Exit"; $bot->cmd(106,0,'');});
glutMouseWheelFunc(\&mousewheel);
glutMotionFunc(\&mousemove);
glutMouseFunc(\&mouseaction);
glutSpecialFunc(\&keyboard);
glutKeyboardFunc(\&keyboard);

glEnable(GL_BLEND);
glBlendFunc (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
glClearColor(2/256, 18/256, 28/256, 1);

glutMainLoop();


sub run {
	$bot->proc();
	draw();
}
sub draw {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	for(keys(%{$bot->{'entities'}})) {
		if(
			$bot->{'entities'}{$_}{'x'}+$entitysize{$bot->{'entities'}{$_}{'type'}}<$pos[0]-$viewport[0] ||
			$bot->{'entities'}{$_}{'y'}+$entitysize{$bot->{'entities'}{$_}{'type'}}<$pos[1]-$viewport[1] ||
			$bot->{'entities'}{$_}{'x'}-$entitysize{$bot->{'entities'}{$_}{'type'}}>$pos[0]+$viewport[0] ||
			$bot->{'entities'}{$_}{'y'}-$entitysize{$bot->{'entities'}{$_}{'type'}}>$pos[1]+$viewport[1]
		) { next; }
		
		my $color=($bot->{'entities'}{$_}{'id'}==$bot->{'self'}{'id'} ? 0:
			($bot->{'players'}{$bot->{'entities'}{$_}{'id'}}{'ally'} ? 1:2)
		);
		entity($bot->{'entities'}{$_}{'x'}-$pos[0], $pos[1]-$bot->{'entities'}{$_}{'y'}, $bot->{'entities'}{$_}{'type'}, $color);
	}
	
	glColor4f(1,1,1, 1);
	glPrint('('.$pos[0].','.$pos[1].') Scale: '.$scale.', Spawn: '.$entity{$spawn[$ind]});
	glutSwapBuffers();
}
sub entity {
	my($x, $y, $type, $ally)=@_;
	
	glBegin(GL_TRIANGLE_FAN);
	glColor4f(@{$entcolor[$ally]}, 0.3);
	glVertex2f($x,$y);
	for(my $i=0; $i<=16; $i++) {
		glVertex2f($x+sin($i*pi/8)*$entitysize{$type}, $y+cos($i*pi/8)*$entitysize{$type});
	}
	glEnd();
	
	glBegin(GL_LINE_STRIP);
	glColor4f(0.1, 0.1, 0.1, 0.7);
	for(my $i=0; $i<=16; $i++) {
		glVertex2f($x+sin($i*pi/8)*$entitysize{$type}, $y+cos($i*pi/8)*$entitysize{$type});
	}
	glEnd();
}

sub glPrint {
	my($str)=@_;
	my @c=split('', $str);
	
	#glRasterPos2i(-1, 1);
	glRasterPos2f(-$viewport[0], $viewport[1]);
	foreach(@c) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord($_));
	}
}

sub mouseaction {
	my($b, $s, $x, $y)=@_;
	if($s!=GLUT_DOWN) { return; } #Only care about mouse down
	
	switch($b) {
		case GLUT_LEFT_BUTTON {
			if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
				@mouse=($x, $y);
			} else {
				$bot->queuecmd($bot->buildx($spawn[$ind],
					$pos[0]+($x/($winsize[0]/2)-1)*$viewport[0],
					$pos[1]-($y/($winsize[1]/2)-1)*$viewport[1], 
				0), 0, 1);
			}
		}
		case GLUT_RIGHT_BUTTON {
			my $cx=$pos[0]+($x/($winsize[0]/2)-1)*$viewport[0];
			my $cy=$pos[1]-($y/($winsize[1]/2)-1)*$viewport[1];
			for(keys(%{$bot->{'entities'}})) {
				if(
					($bot->{'entities'}{$_}{'x'}-$cx)**2+
					($bot->{'entities'}{$_}{'y'}-$cy)**2>
					$entitysize{$bot->{'entities'}{$_}{'type'}}**2
				) { next; }
				
				$bot->queuecmd($bot->del($cx, $cy-$entitysize{$bot->{'entities'}{$_}{'type'}}, 0), 0, 1);
				last;
			}	
		}
	}
}
sub mousewheel {
	my($b, $d)=@_;
	if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
		$scale*=$d>0 ? 1.1:0.9;
		resize(@winsize);
	} else {
		$ind+=$d>0 ? 1:-1;
		if($ind>=@spawn) { $ind=0; }
		if($ind<0) { $ind=@spawn-1; }
	}
}
sub mousemove {
	my($x, $y)=@_;
	
	if(glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
		$pos[0]+=($mouse[0]-$x)*2/($winsize[0]*$scale);
		$pos[1]+=($y-$mouse[1])*2/($winsize[1]*$scale);
		@mouse=($x, $y);
	}
}
sub keyboard {
	my($k, $x, $y)=@_;
}
sub resize {
	my($w, $h)=@_;
	
	@winsize=($w, $h);
	@viewport=($w/($szdiv*$scale), $h/($szdiv*$scale));
	glViewport(0,0,$w, $h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-$viewport[0], $viewport[0], $viewport[1], -$viewport[1], 0, 1);
	glMatrixMode (GL_MODELVIEW);
	glLoadIdentity();
}