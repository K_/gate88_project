use strict;
use warnings;

use OpenGL qw(:all);
use Math::Trig;

my @winsize=(500, 500);
my @pos=(0, 0);
my @mouse=(0, 0);
my %entsize=(
	2=>0.25
);
my @entcolor=(
	[0, 53/256, 0], 
	[0, 50/256, 130/256], 
	[107/256, 0, 1/256]
);
my @entities=(1);

glutInit();

glutDisplayFunc(\&draw);
glutIdleFunc(\&draw);

glutInitWindowSize($winsize[0], $winsize[1]);
my $win=glutCreateWindow('Game View');
glutMotionFunc(\&mouse);
glutMouseFunc(sub {
	my($b, $s, $x, $y)=@_;
	#if($b!=GLUT_LEFT_BUTTON || $s!=GLUT_DOWN) { return; }

	@mouse=($x, $y);
});

glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(-1, 1, 1, -1, 0, 1);
glMatrixMode (GL_MODELVIEW);
glClearColor(2/256, 18/256, 28/256, 1);

glutMainLoop();

sub draw {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	for(my $i=0; $i<@entities; $i++) {
		#if(blaah) { next; }
		entity(0-$pos[0],0+$pos[1], 2, 0);
	}
	
	glColor4f(1,1,1, 1);
	glPrint($pos[0].','.$pos[1]);
	glutSwapBuffers();
}

sub mouse {
	my($x, $y)=@_;

	$pos[0]+=($mouse[0]-$x)*2/$winsize[0];
	$pos[1]+=($y-$mouse[1])*2/$winsize[1];
	@mouse=($x, $y);
}

sub entity {
	my($x, $y, $type, $ally)=@_;

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(@{$entcolor[$ally]}, 1);
	
	glVertex2f($x,$y);
	for(my $i=0; $i<=12; $i++) {
		glVertex2f($x+sin($i*pi/6)*$entsize{$type}, $y+cos($i*pi/6)*$entsize{$type});
	}
	glEnd();
}

#DBG
sub t {
	my($a)=@_;
	my @x=split(/ /, $a);
	for(my $i=0; $i<@x; $i++) {
		$x[$i]/=256;
	}
	return @x;
}
sub glPrint {
	my($str)=@_;
	my @c=split('', $str);
	
	glRasterPos2i(-1, 1);
	foreach(@c) {
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord($_));
	}
}
