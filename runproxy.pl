#!/usr/bin/perl
use Modern::Perl;

use Gate88::Proxy;

my $proxy = Gate88::Proxy->new();
$proxy->run();