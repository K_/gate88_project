Gate88 Project
==============

This repo contains several servers/clients that interface with the Gate88 game. An overview of the major parts of this repo is given below:

Gate88
------

This directory contains all of the logic for the various sub projects.

- `Bot` - A base class for bot players 
- `MasterServer` - Emulates the Gate88 Master Server and allows players to discover servers that are currently online.
- `Server` - Emulates a Gate88 game server. This should hopefully allow custom game modes in the future.
- `Proxy` - Proxies a Gate88 client connection to a game server. Useful for reversing the network protocol for the game.
- `ServerList` - A small client that periodically queries the list of servers and serializes it to a file.

Notes
-----

Contains bit and pieces of information on the game binary as well as the network protocol for the game. Additional documentation for this project will be placed here.

Tools
-----

Several tools to ease the development of the code.

Old
---

Old code from previous versions of the code. Largely useless.

Test
----

This directory contains several tests.