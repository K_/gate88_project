#!/usr/bin/perl
#List all packets and their fields
use Modern::Perl;

use lib '..';

use Tools::GenCommandDocs::Fields;
use Tools::GenCommandDocs::Notes;
use Gate88::Core;

for(my $i = 0; $i < @Gate88::Core::Command::TABLE; ++$i) {
    if(defined($Gate88::Core::Command::TABLE[$i])) {
        my $module = $Gate88::Core::Command::TABLE[$i];
        my $type = $i;
        my $p_size = $module->P_SIZE;
        my $important = $module->IMPORTANT;

        my $obj = $module->new();

        printf("%02x:%04x $module : %s\n", $type, $p_size, $important ? 'Important':'');
        #Print each field
        foreach my $field (@{$obj->FIELDS}) {
            my $doc = defined($fields{$field}) ? $fields{$field}[0]:'NO DOCS';
            print "    $field - $doc\n";

            #Print enums
            if(defined($fields{$field}[1])) {
                my $enum = $fields{$field}[1];
                foreach my $key (keys(%$enum)) {
                    print "        $key - $enum->{$key}\n";
                }
            }
        }
        #If there are notes, print those too.
        if(defined($notes{$type})) {
            print "\t" . $notes{$type} . "\n";
        }
        print "\n";
    }
}