Tools
=====

CodeCleanup
-----------
A once off script that cleans up whitespace in the code.

FindFixmes
----------
Searches for several strings (FIXME, HACK, etc) which indicate that the code needs additional work.

GenCommandDocs
--------------
Generates documentation of every `Command` given the data in the `GenCommandDocs` directory and the data in each `Command` class.

Templates
---------
Templates for various classes.