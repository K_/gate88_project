use Modern::Perl;

use File::Slurp;

if(!@ARGV) {
    die('No file specified');
}

my $data = read_file($ARGV[0]);

my $class = ($data =~ /package (.+);/)[0];
my @mtds = $data =~ /sub (\w+) {\n\s+my\((.+?)\)/g;


printf("%s\n", $class);
for(my $i = 0; $i < @mtds; $i += 2) {
    printf("    %s(%s)\n", $mtds[$i], $mtds[$i + 1]);
}
