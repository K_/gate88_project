#!/usr/bin/perl
package Tools::GenCommandDocs::Notes;
use Modern::Perl;
use base 'Exporter';

our @EXPORT = ('%notes');

#Commentary on commands
our %notes = (
    3   => 'There appears to be no indication of this in the client binary...',
    7   => 'Accepts up to 200 ips, but the client throws an assert fail if count>199',
    101 => 'This message must be sent before the server acknoledges that you are playing!',
    103 => 'You can build even if a prereq is not met',
    142 => 'Player id is the "owner" or the person who got the points',
);
1;