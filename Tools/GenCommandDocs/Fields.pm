#!/usr/bin/perl
package Tools::GenCommandDocs::Fields;
use Modern::Perl;
use base 'Exporter';

our @EXPORT = ('%fields');

#Documentation on what the different fields are
our %fields = (
    'unknown'   => ['Unknown data'],
    'unknown1'  => ['Unknown data'],
    'unknown2'  => ['Unknown data'],
    'unknown3'  => ['Unknown data'],
    'unknown4'  => ['Unknown data'],

    'upgrade'   => ['ID of the upgrade', {
                    0   => 'Battery',
                    1   => 'Mechanics',
                    2   => 'Heavy Materials',
                    3   => 'Weapon Pod',
                    4   => 'Signals',
                    5   => 'Mini Turret',
                    6   => 'Regen Config',
                    7   => 'Heavy Config',
                    8   => 'Normal Config',
                    9   => 'Stealth Config',
                   }],
    'name'      => ['Name of player'],
    'port'      => ['Port of the server'],
    'page'      => ['Server list page'],
    'count'      => ['Number of results'],
    'type'      => ['Type id of command'],
    'version'   => ['Version of the client/server'],

    'player_id' => ['ID of the player'],
    'entity_id' => ['ID of the source entity'],
    'target_id' => ['ID of the target entity'],
    'yard_id'   => ['ID of the yard'],

    'g_counter' => ['Global ack counter'],
    'counter'   => ['Command specific counter'],

    'x'         => ['X coordinate'],
    'y'         => ['Y coordinate'],
    'x_angle'   => ['X angle cos(a)'],
    'y_angle'   => ['Y angle sin(a)'],
    'x_velocity'=> ['X velocity of entity'],
    'y_velocity'=> ['Y velocity of entity'],

    'hp'        => ['Remaining HP of entity'],
    'cloak'     => ['State of unit cloak'],
    'special'   => ['State of ship special'],
    'style'     => ['Player ship style'],
    'key'       => ['Key being modified'],
    'keys'      => ['State of keys'],
    'time'      => ['Time remaining in round'],
    'message'   => ['Text being sent'],
    'ping'      => ['Latency in ms'],
    'kills'     => ['Number of ship kills'],
    'deaths'    => ['Number of ship deaths'],
    'cc_kills'  => ['Number of CC kills'],
    'cc_deaths' => ['Number of CC deaths'],
    'resources' => ['Resource count'],
    'max_resources'=>['Resource limit'],
    'angle'     => ['Angle of the entity'],
    'level'     => ['Level of upgrade being researched'],
    'order'     => ['Command sent to yards'],
    'group'     => ['Group id to order', {
                    0   => 'Red',
                    1   => 'Green',
                    2   => 'Blue',
                   }],
    'news'      => ['Master server news'],
    'players'   => ['Number of players on the server'],
    'max_players'=>['Maximum number of players allowed on the server'],
    'entity_type'=>['Type of entity being created', {
                    3   => 'Power Gen',
                    4   => 'Lab',
                    5   => 'Factory',
                    6   => 'Bomb',
                    7   => 'Signal Station',
                    8   => 'Jump Gate',
                    10  => 'Turret',
                    11  => 'Mass Driver',
                    14  => 'Regen Turret',
                    15  => 'Exciter Turret',
                    20  => 'Fighter Yard',
                    21  => 'Bomber Yard',
                   }],
);
1;