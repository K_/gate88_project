#Remove whitespace at end of lines
find Gate88 -type f -exec perl -pi -e 's/[ \t]+$//g' {} ';'

#Replace tabs with spaces
find Gate88 -type f -exec perl -0pi -e 's/\t/    /g' {} ';'

#Replace windows EOL with linux
find Gate88 -type f -exec perl -0pi -e 's/\r\n/\n/g' {} ';'
find Gate88 -type f -exec perl -0pi -e 's/\r/\n/g' {} ';'
