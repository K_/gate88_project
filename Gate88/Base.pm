package Gate88::Base;

use Modern::Perl;

use IO::Socket::INET;
use Math::Trig;
use Time::HiRes qw(sleep gettimeofday tv_interval);
use Term::ReadKey; #for terminal control
use IO::Handle qw(); #for flushing STDOUT

use Gate88::Core;

sub new {
    my $class = shift;
    my($s) = @_;

    my $self = bless({
        'nm' => Gate88::Core::NetworkManager->new($s),
        'tm' => Gate88::Core::TimerManager->new(),
        'time' => [gettimeofday()],

        'commands' => {},
        'input' => [],

        'running' => 1,
    }, $class);

    return $self;
}

sub run {
    my($s) = @_;

    while($s->proc()) { sleep(0.1); }
}

sub proc {
    my($s) = @_;
    my $time = [gettimeofday()];
    my $intv = tv_interval($s->{'time'}, $time);
    $s->{'time'} = $time;

    $s->{'nm'}->proc($intv, $s->{'gs'});
    $s->{'tm'}->proc($intv, $s->{'gs'}, $s->{'nm'});
    $s->{'nm'}->flush($intv, $s->{'gs'});

    $s->_procinput;

    return $s->{'running'};
}

sub addcommand {
    my($s, $name, $callback) = @_;

    $s->{'commands'}{$name} = $callback;
}

sub addcmdcallback {
    my($s, $type, $callback) = @_;

    $s->{'nm'}->addcallback($type, sub { shift; $s->$callback(@_); });
}

sub addintvcallback {
    my($s, $intv, $callback) = @_;

    $s->{'tm'}->addcallback($intv, sub { shift; $s->$callback(@_); });
}

sub settimeoutcallback {
    my($s, $callback) = @_;

    $s->{'nm'}->settimeoutcallback(sub { shift; $s->$callback(@_); });
}

sub _procinput {
    my($s) = @_;

    if(!@{$s->{'input'}}) {
        push($s->{'input'}, '');
    }
    while (defined (my $key = ReadKey(-1))) {
        if( $key ne "\n" && $key ne "\r") {
            $s->{'input'}[-1] .= $key;
        } else {
            push($s->{'input'}, '');
        }
    }
    STDOUT->flush();

    while(@{$s->{'input'}} > 1) {
        my $cmd = shift(@{$s->{'input'}});
        $s->_parsecmd($cmd);
    }
}

sub _parsecmd {
    my($s, $cmd) = @_;

    my($name, $data) = split(/ /, $cmd, 2);
    if($name && defined($s->{'commands'}{$name})) {
        $s->{'commands'}{$name}($s, $data);
    }
}
1;
