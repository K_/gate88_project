package Gate88::ServerList;
use parent qw/Gate88::Base/;

use Modern::Perl;

use IO::Socket::INET;
use Text::CSV;

sub new {
    my $class = shift;

    my $s = IO::Socket::INET->new(Proto => 'udp', Blocking => 0) or die($!);
    my $self = $class->SUPER::new($s);

    $self->{'servers'} = {};
    $self->{'count'} = 0;
    $self->{'timeout'} = 0;

    $self->registercallbacks();

    my $host = gethostbyname('localhost');
    my $sin = sockaddr_in(29981, $host);
    my $conn = Gate88::Core::Connection->new($s->peeraddr, $s->peerport);
    $conn->{'persistent'} = 1;
    $conn->{'type'} = Gate88::Core::Connection::MSERVER;
    $self->{'connections'}{$sin} = $conn;
    $self->{'mserver'} = $sin;

    $self->querylist();

    return $self;
}

sub registercallbacks {
    my($s) = @_;

    $s->addcmdcallback(Gate88::Core::Command::ServerListSend::TYPE, \&serverlist_s);
    $s->addcmdcallback(Gate88::Core::Command::ServerInfoSend::TYPE, \&serverinfo_s);

    $s->addintvcallback(3, \&writelist);
    $s->addintvcallback(30, \&querylist);

    $s->addcommand('q', \&command_quit)
}

sub command_quit {
    my($s) = @_;

    $s->{'running'} = 0;
}

sub querylist {
    my($s) = @_;

    my $g_counter = $s->{'nm'}->g_counter();

    Gate88::Core::Logger::log('Requesting list of servers', 0);
    $s->queuecmdmserver(Gate88::Core::Util::CommandBuilder::ServerListRequest($g_counter, 0), 0);
}

sub writelist {
    my($s) = @_;

    if($s->{'timeout'} < 0) {
        ++$s->{'timeout'};
        return;
    }

    if($s->{'timeout'} == 0) {
        Gate88::Core::Logger::log('Writing list', 0);

        my $out = $s->_printservers($s->{'servers'});

        ++$s->{'timeout'};
    }
}

sub serverlist_s {
    my($s, $cmd, $src) = @_;
    my $page = $cmd->page;
    my $g_counter = $s->{'nm'}->g_counter();

    $s->{'connections'}{$src}->{'type'} = Gate88::Core::Connection::CLIENT;

    if($cmd->page == 0) {
        $s->{'servers'} = {};
        $s->{'count'} = 0;
        $s->{'timeout'} = 0;
    }

    if($cmd->count) {
        my $servers = $s->_parselist($cmd);

        foreach my $key (@$servers) {
            $s->{'servers'}{$key} = 0;
            $s->{'nm'}->queuecmd(Gate88::Core::Util::CommandBuilder::ServerInfoRequest(
                $s->{'nm'}->counter(Gate88::Core::Command::ServerInfoRequest::TYPE)
            ), $key, 0);
        }
        $s->queuecmdmserver(Gate88::Core::Util::CommandBuilder::ServerListRequest($g_counter, $cmd->page + 1), 0);
    }

    Gate88::Core::Logger::log("Received page: " . $page, 0);
}

sub serverinfo_s {
    my($s, $cmd, $src) = @_;

    $s->{'servers'}{$src} = $s->_parseinfo($cmd);
    $s->{'connections'}{$src}->{'type'} = Gate88::Core::Connection::SERVER;
}

sub _parselist {
    my($s, $cmd) = @_;

    my $ret = [];
    my $glob = $cmd->glob;
    for(my $i = 0; $i < 198 && $i < $cmd->count; ++$i) {
        my ($ip, $port) = unpack('a[4]S>', substr($glob, $i * 6, 6));
        my $key = sockaddr_in($port, $ip);

        push(@$ret, $key);
    }
    return $ret;
}

sub _parseinfo {
    my($s, $cmd) = @_;

    my %ret = %{$cmd->{'fields'}};

    $ret{'name'} = $cmd->_name;

    return \%ret;
}

sub _printservers {
    my($s, $svrs) = @_;

    my $csv = Text::CSV->new();
    open(my $fh, '>', 'list.csv');

    for my $key (keys(%$svrs)) {
        my $svr = $svrs->{$key};
        if(!$svr) { next;}

        $csv->print($fh, [$svr->{'name'}, $svr->{'players'} . '/' . $svr->{'max_players'}, $svr->{'time'}]);
    }

    close($fh);
}

sub queuecmdmserver {
    my($s, $cmd, $delay) = @_;

    $s->{'nm'}->queuecmd($cmd, $s->{'mserver'}, $delay);
}

1;