package Gate88::Proxy;
use parent qw/Gate88::Base/;

use Modern::Perl;

use IO::Socket::INET;

sub new {
    my $class = shift;

    my $s = IO::Socket::INET->new(LocalPort => 8888, Proto => 'udp', Blocking => 0) or die($!);
    my $self = $class->SUPER::new($s);
    $self->{'nm'}->{'autoack'} = 0;
    $self->{'nm'}->{'autoresend'} = 0;

    $self->registercallbacks();

    my $host = gethostbyname('localhost');
    my $sin = sockaddr_in(29987, $host);
    my $conn = Gate88::Core::Connection->new($s->peeraddr, $s->peerport);
    $conn->{'persistent'} = 1;
    $conn->{'type'} = Gate88::Core::Connection::SERVER;
    $self->{'nm'}->addconn($sin, $conn);

    return $self;
}

sub registercallbacks {
    my($s) = @_;

    #Add custom callbacks here
    $s->addcmdcallback(Gate88::Core::Command::KeyUpdate::TYPE, \&x130);
    $s->addcmdcallback(Gate88::Core::Command::KeysNotify::TYPE, \&x130);
    $s->addcmdcallback(Gate88::Core::Command::ShipStatusNotify::TYPE, \&key_check);
#    $s->addcmdcallback(Gate88::Core::Command::ServerConnect::TYPE, \&serverconn);
#    $s->addcmdcallback(Gate88::Core::Command::EntityCreateNotify::TYPE, \&ent_log);
#    $s->addcmdcallback(Gate88::Core::Command::CommandAcknowledge::TYPE, \&ack_log);
#    $s->addcmdcallback(Gate88::Core::Command::UNKNOWN130::TYPE, \&cmd_dump);
#    $s->addcmdcallback(Gate88::Core::Command::UNKNOWN147::TYPE, \&cmd_dump);
#    $s->addcmdcallback(Gate88::Core::Command::KeyUpdate::TYPE, \&cmd_dump);

    #Add callback for every command
    for(my $i = 0; $i < 150; ++$i) {
        if(!$s->{'nm'}->{'callbacks'}[$i]) {
            $s->addcmdcallback($i, \&forward);
        }
    }
}

sub serverconn {
    my($s, $cmd, $src) = @_;

    $s->{'connections'}{$src}->{'type'} = Gate88::Core::Connection::CLIENT;

    $s->forward($cmd, $src);
}

sub x130 {
    my($s, $cmd, $src) = @_;

    Gate88::Core::Logger::log($cmd->dump(), 0);
    $s->forward($cmd, $src);
}

sub x147 {
    my($s, $cmd, $src) = @_;

    Gate88::Core::Logger::log($cmd->dump(), 0);
    $s->forward($cmd, $src);
}

sub ent_log {
    my($s, $cmd, $src) = @_;

    my $entity = Gate88::Core::Util::EntityParser::parse($cmd);

    if($entity) {
        my $type = $entity->NAME;
        my $player_id = $cmd->player_id;
        my $id = $entity->{'id'};
        my $x = $entity->x;
        my $y = $entity->y;
        Gate88::Core::Logger::log("Spawned: $type, ID: $id, Pos: ($x, $y), Player: $player_id", 0);
    }
    $s->forward($cmd, $src);
}

sub key_check {
    my($s, $cmd, $src) = @_;

    Gate88::Core::Logger::log($cmd->keys, 1);

    $s->forward($cmd, $src);
}

sub ack_log {
    my($s, $cmd, $src) = @_;

    my $important = 0;
    if(defined($Gate88::Core::Command::TABLE[$cmd->type])) {
        my $module = $Gate88::Core::Command::TABLE[$cmd->type];
        $important = $module->IMPORTANT;
    }

    if(!$important) {
        Gate88::Core::Logger::log(sprintf("Missing ack for [%02x]", $cmd->type), 0);
    }
    $s->forward($cmd, $src);
}

sub cmd_dump {
    my($s, $cmd, $src) = @_;

    open(my $fh, '>>', 'Dump.log');
    my $str = ($s->{'nm'}{'connections'}{$src}->{'type'} == Gate88::Core::Connection::CLIENT ? "CLIENT":"SERVER") . ": ";
    $str .= $cmd->dump();

    Gate88::Core::Logger::log($str, 0);
    print $fh $str . "\n";
    close($fh);

    $s->forward($cmd, $src);
}

sub forward {
    my($s, $cmd, $src) = @_;

    $s->{'nm'}->queuecmdbroadcast($cmd, [$src], [], 0);
}
1;
