package Gate88::Bot::Beta;
use parent qw/Gate88::Bot::Base/;

use Modern::Perl;

use Gate88::Bot::State::MoveState;
use Gate88::Bot::State::SleepState;
use Gate88::Bot::State::PlaceCommandPostState;

sub new {
    my $class = shift;
    my($name) = @_;

    my $self = $class->SUPER::new('[Bot] Beta');

	$self->{'ss'}->push(Gate88::Bot::State::MoveState->new(-0.75,2.0));
	$self->{'ss'}->push(Gate88::Bot::State::PlaceCommandPostState->new(1.0, -0.25));
	$self->{'ss'}->replace(Gate88::Bot::State::SleepState->new(2.0));

    return $self;
}

1;