package Gate88::Bot::One;
use parent qw/Gate88::Bot::Base/;

use Modern::Perl;

use Gate88::Bot::Core::AllianceManager;
use Gate88::Bot::Core::BuildManager;
use Gate88::Bot::Core::KeyManager;
use Gate88::Bot::Core::LocationManager;
use Gate88::Bot::Core::RestartManager;

use Gate88::Bot::State::SetupState;

sub new {
    my $class = shift;

    my $self = $class->SUPER::new('[Bot] One');

    $self->{'ss'}->push(Gate88::Bot::State::SetupState->new());

    $self->add_manager('am', Gate88::Bot::Core::AllianceManager->new($self));
    $self->add_manager('bm', Gate88::Bot::Core::BuildManager->new($self));
    $self->add_manager('km', Gate88::Bot::Core::KeyManager->new($self));
    $self->add_manager('rm', Gate88::Bot::Core::RestartManager->new($self));
    $self->add_manager('lm', Gate88::Bot::Core::LocationManager->new($self));

    return $self;
}

1;
