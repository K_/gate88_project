package Gate88::Bot::State::MoveState;
use parent qw/Gate88::Bot::State/;

use Modern::Perl;

use feature qw(switch);
#For 2D vectors, see usage here:
#http://search.cpan.org/~salva/Math-Vector-Real-0.10/lib/Math/Vector/Real.pm
use Math::Vector::Real;
use Math::Trig ':pi';
use Gate88::Core::Util::Function;
use Gate88::Bot::Core::KeyManager;
use Gate88::Bot::Core::MicroState;

use constant {
	'DEGTORAD' => pi/180.0,
	'RADTODEG' => 180.0/pi,
	'TRUE' => 1,
	'FALSE' => 0,
};

use constant {
    'STOP' => 0.00001,
	'MAX_ANG_DIFF' => 25*DEGTORAD,		#turns too much
	'MIN_BTN_PRESS_T' => 0.4,			#left/right button gets stuck if it's pressed shorter time than that
	'SHIP_SPEED' => 0.5,				#will have to get this from the ship object later.. because of ship updates
	'SHIP_ANG_VEL' => 143.5*DEGTORAD,	#rad/sec
	'ANG_UPDATE_T' => 0.3,				#seconds
	'STOP_DISTANCE' => 0.225,			#the distance it takes to stop from full speed
};

#MoveState: move to the given coordinates and stop there
#Complete implementation concept:
#If the target is far, use Side+Forward keys at once to get there faster
#If it's very close, stop, and use only side keys to turn to the point, then start moving carefully
#If the ship is pushed while moving, turn to the right angle again and move that direction
#Start braking before reaching the point

#What is done from all the above so far:
#Turn smoothly (with key+set angle) towards target point
#Start moving and stop at point
#so not much yet

sub new {
    my $class = shift;
    my($x, $y) = @_;

    my $self = $class->SUPER::new();
    $self->{'dest'} = V($x, $y);
	$self->{'keyboard'} = undef;
	$self->{'timer'} = Gate88::Bot::Core::Timer->new();
	$self->{'ustate'} = Gate88::Bot::Core::MicroState->new();

    return $self;
}

sub init {
	my($s, $ss, $gs, $nm, $tm) = @_;
	$s->{'keyboard'} = Gate88::Bot::Core::KeyManager->new( $gs, $nm );
}

sub enter {
	my($s, $ss, $gs, $nm, $tm) = @_;
}

my $init_ang_diff;

sub proc {
    my($s, $ss, $gs, $nm, $tm) = @_;

    my $ship = $gs->{'self'}{'ship'};
	my $diff = V( $s->{'dest'}->[0] - $ship->x, $s->{'dest'}->[1] - $ship->y);
    my $dist = get_length( $diff );
	#param order is atan2( Y, X)
    my $desired_angle = atan2( $diff->[1], $diff->[0]);

	my $ang_diff = get_ang_diff( $ship->angle, $desired_angle);
	
	#print "======================\n";
	#print "Ship ang: ".$ship->angle*RADTODEG."\n";
	#print "Target ang: ".$desired_angle*RADTODEG."\n";
	#print "Ang diff: ".$ang_diff*RADTODEG."\n";

	given ( $s->{'ustate'}->get_active() )
	{
		when (0) {
			print ".";
		}
		when (1) {
			#You can't really use the arrows precisely when you need only a little change in the angle
			#But then the server will swap to the angle in ShipUpdate packet
			if( $s->{'ustate'}->once() ) {
				$init_ang_diff = $ang_diff;
				
				if($ang_diff < -1.0*MAX_ANG_DIFF) {
					$s->{'keyboard'}->press_right();
				} elsif($ang_diff > MAX_ANG_DIFF) {
					$s->{'keyboard'}->press_left();
				}
			}

			my $hold_time = abs($init_ang_diff) / SHIP_ANG_VEL;
			if( $hold_time < MIN_BTN_PRESS_T ) { $hold_time = MIN_BTN_PRESS_T; }
			if( $s->{'ustate'}->check_elapsed($hold_time-0.05) ) {
				$s->{'keyboard'}->release_side();
				$s->{'ustate'}->next();
				print "Stopped rotating.\n";
			}
		}
		when (2) {	#Got the angle rouhly with the keys, now set it precisely
			print "Set Key prec.\n";
			$nm->queuecmdbroadcast(Gate88::Core::Util::CommandBuilder::ShipUpdate(
				cos($desired_angle), sin($desired_angle), $ship->x, $ship->y, 0, 0
			), [], [Gate88::Core::Connection::SERVER], 0);
			print "Got angle precisely.\n";
			$s->{'ustate'}->next();		#_delayed(0.05);
			$s->{'timer'}->set(ANG_UPDATE_T);
		} when (3) {	#Now let's go
			#TODO: now it turns back to the correct angle if loses it, but will need some thrust to go in that direction
			if( $s->{'timer'}->check() ) {
				#Position is ignored if it's not very different from the position the server thinks
				#So we set only the angle
				$nm->queuecmdbroadcast(Gate88::Core::Util::CommandBuilder::ShipUpdate(
					cos($desired_angle), sin($desired_angle), $ship->x+$ship->x_velocity* 0.2, $ship->y+$ship->x_velocity* 0.2, 0, 0
				), [], [Gate88::Core::Connection::SERVER], 0);
				$s->{'timer'}->set(ANG_UPDATE_T);
			}
			if( $s->{'ustate'}->once() ) {
				$s->{'keyboard'}->press_up();
			}
			if( $s->{'ustate'}->check_elapsed(1.0) ) {	#~reached full speed
				$s->{'keyboard'}->release_up();
			}
			#print "Distance: ".$dist."\n";
			if( $dist <= STOP_DISTANCE) {
				$s->{'keyboard'}->brake();
				$s->{'ustate'}->next_delayed(1.0);
			}
		} when (4) {
			#we're there
			if( $s->{'ustate'}->once() ) {
				$s->{'keyboard'}->release_down();
				my $pos = V( $ship->x, $ship->y);
				print "Final distance: ".$dist."\n";
				print "Final position: ".$pos."\n";
			}
		}
		default { print "0\n"; }
	}
	
    return 1;	#return 1 -> stay in this State
}

1;