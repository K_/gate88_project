package Gate88::Bot::State::BuildState;
use parent qw/Gate88::Bot::State/;

use Modern::Perl;

use Math::Trig;

sub new {
    my $class = shift;    
    my($list) = @_;

    my $self = $class->SUPER::new();
    $self->{'list'} = $list;

    return $self;
}

sub init {
    my($s, $ss, $gs, $nm, $tm) = @_;

    #Normalize all coordinates to use abs positioning
    for(my $i = 0; $i < @{$s->{'list'}}; ++$i) {
        my $arr = $s->{'list'}[$i];
        if(!defined($arr->[3])) {
            next;
        }

        my $entity = $gs->{'entities'}->get($arr->[3]);

        if(!defined($entity)) {
            $arr->[3] += undef;
            next;
        }

        $arr->[1] += $entity->x;
        $arr->[2] += $entity->y;
        $arr->[3] += undef;
    }

    $ss->add_handler($s, Gate88::Core::Command::EntityCreate::TYPE, \&_ent_create_n);
}

#positioning may be relative or absolute
sub proc {
    my($s, $ss, $gs, $nm, $tm) = @_;

    if(!$s->{'HACK'}) {
        print "BUILD\n";
        $s->{'HACK'} = 1;
        foreach my $ent (@{$s->{'list'}}) {
            $s->_sendbuild($nm, $ent->[0], $ent->[1], $ent->[2]);
        }
    }
    return 1;
}

sub _surround {
    my($s, $x, $y, $type, $count, $dist, $pos) = @_;

    my @list = ();

    for(my $i = 0; $i < $count; ++$i) {
        my $ang = 2 * pi * ($i / $count);
        my $dx = sin($ang) * $dist;
        my $dy = cos($ang) * $dist;

        push(@list, [$type, $x + $dx, $y + $dy, $pos]);
    }

    return @list;
}

sub _sendbuild {
    my($s, $nm, $type, $x, $y) = @_;

    $nm->queuecmdbroadcast(Gate88::Core::Util::CommandBuilder::EntityCreate(
        $nm->g_counter(), $type, $x, $y, $nm->counter(Gate88::Core::Command::EntityCreate::TYPE)
    ), [], [Gate88::Core::Connection::SERVER], 0);
}

sub _entcreate_n {
    my($s, $nm, $evt, $gs) = @_;

    if($evt->player_id != $gs->{'self'}->id) {
        return;
    }

    
}
1;
