package Gate88::Bot::State::SleepState;
use parent qw/Gate88::Bot::State/;

use warnings;
use strict;

use Gate88::Bot::Core::Timer;

sub new {
    my $class = shift;
    my $duration_sec = shift;

    my $self = $class->SUPER::new();

	$self->{'duration_sec'} = $duration_sec;
	$self->{'timer'} = Gate88::Bot::Core::Timer->new();
	
    return $self;
}

sub init {
	my($s, $ss, $gs, $nm, $tm) = @_;
}

sub enter {
	my($s, $ss, $gs, $nm, $tm ) = @_;
	$s->{'timer'}->set($s->{'duration_sec'});
	print "Enter SS\n";
}

sub proc {
    my($s, $ss, $gs, $nm, $tm) = @_;
	return !($s->{'timer'}->check());
}

1;