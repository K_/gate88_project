package Gate88::Bot::State::SetupState;
use parent qw/Gate88::Bot::State/;

use Modern::Perl;

use Gate88::Bot::State::MoveState;
use Gate88::Bot::State::PlaceCommandPostState;
use Gate88::Bot::State::BuildFarmState;
use Gate88::Bot::State::MainState;

sub proc {
    my($s, $ss, $gs, $nm, $tm) = @_;

    #If the ship doesn't exist, wait.
    if(!$gs->{'self'}->{'ship'}) {
        return 1;
    }

    my $base_x = rand()*10 - 5;
    my $base_y = rand()*10 - 5;
    my $farm_x = rand()*10 - 5;
    my $farm_y = rand()*10 - 5;

    $ss->replace(Gate88::Bot::State::MainState->new([$base_x, $base_y], [$farm_x, $farm_y]));
    $ss->push(Gate88::Bot::State::BuildFarmState->new($farm_x, $farm_y));
    $ss->push(Gate88::Bot::State::MoveState->new($farm_x, $farm_y, 0.8));
    $ss->push(Gate88::Bot::State::PlaceCommandPostState->new($base_x, $base_y));;
    $ss->push(Gate88::Bot::State::MoveState->new($base_x, $base_y, 0.5));

    return 1;
}

1;
