package Gate88::Bot::State::PlaceCommandPostState;
use parent qw/Gate88::Bot::State/;

use Modern::Perl;

sub new {
    my $class = shift;
    my($x, $y) = @_;

    my $self = $class->SUPER::new();
    $self->{'dest'} = [$x, $y];

    return $self;
}

sub proc {
    my($s, $ss, $gs, $nm, $tm) = @_;

    #If the base exists, pop the state off
    if($gs->{'self'}{'base'}) {
        return 0;
    }

    #Otherwise, send the BaseCreate command
    $nm->queuecmdbroadcast(Gate88::Core::Util::CommandBuilder::BaseCreate(
        $nm->g_counter(),
        $s->{'dest'}[0], $s->{'dest'}[1], 0, 1
    ), [], [Gate88::Core::Connection::SERVER], 0);

    return 1;
}
1;