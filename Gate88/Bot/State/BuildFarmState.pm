package Gate88::Bot::State::BuildFarmState;
use parent qw/Gate88::Bot::State::BuildState/;

use Modern::Perl;

sub new {
    my $class = shift;
    my($x, $y) = @_;

    my $self = $class->SUPER::new([]);

    my @list = ([Gate88::Core::Entity::PowerGen::TYPE, $x, $y, undef]);

    push(@list, $self->_surround(
        $x, $y,
        Gate88::Core::Entity::Lab::TYPE,
        Gate88::Core::Entity::Lab::MAX,
        0.25,
        undef
    ));

    push(@list, $self->_surround(
        $x, $y,
        Gate88::Core::Entity::Factory::TYPE,
        Gate88::Core::Entity::Factory::MAX,
        0.35,
        undef
    ));

    push(@list, [Gate88::Core::Entity::PowerGen::TYPE, $x, $y, undef]);
    $self->{'list'} = \@list;

    return $self;
}

1;
