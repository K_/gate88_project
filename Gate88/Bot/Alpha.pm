package Gate88::Bot::Alpha;
use parent Gate88::Bot;

use Modern::Perl;

use Math::Trig;

sub new {
    my $class = shift;
    my $self = $class->SUPER::new('[Bot]Alpha');

    $self->{'target'} = 0;

    return $self;
};

sub registercallbacks {
    my($s) = @_;

    $s->SUPER::registercallbacks();

    $s->addcmdcallback(Gate88::Core::Command::RoundStartNotify::TYPE, \&roundstart_nai);
    $s->addcmdcallback(Gate88::Core::Command::ShipStatusNotify::TYPE, \&shipstatus_nai);
    $s->addcmdcallback(Gate88::Core::Command::EntityCreateNotify::TYPE, \&entcreate_nai);

    $s->addintvcallback(1.0, \&update);
}

sub update {
    my($s) = @_;

   $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::ShipUpdate($s->{'nm'}->g_counter(),
        0, 1, 0, 0, 0, 1
   ), 0);
}

sub entcreate_nai {
    my($s, $cmd, $src) = @_;

    if($cmd->entity_type == Gate88::Core::Entity::CommandPost::TYPE) {
        if($s->{'gs'}{'self'}{'id'} != $cmd->player_id) {
            $s->{'target'} = $cmd->entity_id;
        } else {

        }
    }
}

my $said_hi = 0;
sub roundstart_nai {
    my($s, $cmd, $src) = @_;
	
	#A good bot says hi
	if(!$said_hi) {
		$s->command_talk("hi all");
		$said_hi = 1;
	}

    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::BaseCreate($s->{'nm'}->g_counter(),
        rand()*2, rand()*2
    ), 5);
    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::KeyUpdate(
        3, 0
    ), 0);
	
}

sub shipstatus_nai {
    my($s, $cmd, $src) = @_;
}

sub spawnbase {
    my($s, $src) = @_;
    my($x, $y) = (0, 0);

    #Exciter
    for(my $i = 0; $i < 15; ++$i) {
        my $angle = 2 * pi * $i/15;
        $s->build(15, cos($angle)*0.22, sin($angle)*0.22);
    }

    #Regen
    for(my $i = 0; $i < 10; ++$i) {
        my $angle = 2 * pi * $i/10;
        $s->build(14, cos($angle)*0.27, sin($angle)*0.27);
    }

    #Driver
    for(my $i = 0; $i < 18; ++$i) {
        my $angle = 2 * pi * $i/18;
        my $type = ($i + 4)% 9 == 0 ? 21 : $i % 3 == 0 ? 20 : 11;
        $s->build($type, cos($angle)*0.5, sin($angle)*0.5);
    }

    #Power
    for(my $i = 0; $i < 60; ++$i) {
        my $angle = 2 * pi * $i/60;
        $s->build($i % 2 == 0 ? 10 : 3, cos($angle)*0.7, sin($angle)*0.7);
    }
}

=cut
sub proc {
    my($s, $b)=@_;

    if(!$b->{'self'}{'id'}) { return; } #Haven't received ID yet, no point in continuing
    if(!@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{2}}) {
        #TEMPPPPPPPPP
        $b->queuecmd(cmd(112,16, s2b($b->{'counter'}{'control'}++).f2b(rand()*5).f2b(rand()*5-0.25).f2b(0).f2b(1)), 0, 1);
        return;
    } #No CC, no point

    if(!$s->{'data'}{'flags'}{'init'}) {
        $s->{'data'}{'flags'}{'init'}=1;
    }

    $s->farm($b);
    $s->research($b);

    if(    !defined($b->{'entities'}{$s->{'data'}{'target'}[0]}) ||
        $b->{'players'}{$b->{'entities'}{$s->{'data'}{'target'}[0]}{'id'}}{'ally'}
    ) { $s->{'data'}{'target'}[0]=0; }

    if(!$s->{'data'}{'target'}[0]) { #Don't run every time, maybe only run on updated ents?
        my @targ=(0,9**9**9);
        foreach(keys(%{$b->{'players'}})) {
            my $id=$_;
            if($b->{'players'}{$id}{'ally'}) { next; } #if allied, don't target them
            foreach(keys(%{$b->{'players'}{$id}{'ents'}})) {
                for(my $i=0; $i<@{$b->{'players'}{$id}{'ents'}{$_}}; $i++) {
                    my $dist=dist(\%{$b->{'entities'}{$b->{'self'}{'ship'}}}, \%{$b->{'entities'}{$b->{'players'}{$id}{'ents'}{$_}[$i]}});
                    if($targ[1]>$dist) { @targ=($b->{'players'}{$id}{'ents'}{$_}[$i], $dist); }
                }
            }
        }
        $s->{'data'}{'target'}=\@targ;
        print "New target: ".$targ[0]."\n";
    }

    if($s->{'data'}{'target'}[0]) {
        my $aa=400;
        my $bb=700;

        $s->{'data'}{'target'}[1]=dist(\%{$b->{'entities'}{$b->{'self'}{'ship'}}}, \%{$b->{'entities'}{$s->{'data'}{'target'}[0]}});
        my $dist=$s->{'data'}{'target'}[1]; #**0.5;
        if($s->{'data'}{'target'}[1]>1) {
            $b->{'entities'}{$b->{'self'}{'ship'}}{'a'}=atan2(
                $b->{'entities'}{$s->{'data'}{'target'}[0]}{'x'}-$b->{'entities'}{$b->{'self'}{'ship'}}{'x'},
                $b->{'entities'}{$s->{'data'}{'target'}[0]}{'y'}-$b->{'entities'}{$b->{'self'}{'ship'}}{'y'}
            );
        } else {
            $b->{'entities'}{$b->{'self'}{'ship'}}{'a'}=atan2( #Goes wonky once dist is high (Due to $aa+$bb*$dist getting really BIG)
                ($b->{'entities'}{$s->{'data'}{'target'}[0]}{'x'}+$b->{'entities'}{$s->{'data'}{'target'}[0]}{'vx'}*($aa+$bb*$dist))-
                ($b->{'entities'}{$b->{'self'}{'ship'}}{'x'}+$b->{'entities'}{$b->{'self'}{'ship'}}{'vx'}*($aa+$bb*$dist)),

                ($b->{'entities'}{$s->{'data'}{'target'}[0]}{'y'}+$b->{'entities'}{$s->{'data'}{'target'}[0]}{'vy'}*($aa+$bb*$dist))-
                ($b->{'entities'}{$b->{'self'}{'ship'}}{'y'}+$b->{'entities'}{$b->{'self'}{'ship'}}{'vy'}*($aa+$bb*$dist))
            );
        }
        #print($aa+$bb*$dist."\n");

        $b->queuecmd(cmd(114,24,
            f2b(sin($b->{'entities'}{$b->{'self'}{'ship'}}{'a'})).
            f2b(cos($b->{'entities'}{$b->{'self'}{'ship'}}{'a'})).
            f2b($b->{'entities'}{$b->{'self'}{'ship'}}{'x'}).
            f2b($b->{'entities'}{$b->{'self'}{'ship'}}{'y'}).
            f2b(0).
            f2b(0)
        ), 0, 0);

        if(!$b->{'buttons'}{'up'}) {
            $b->queuecmd(cmd(102,2,chr(2).chr(0)), 0, 0);
        }
        if($s->{'data'}{'target'}[1]<0.7**2) {
            if(!$b->{'buttons'}{'fire'}) {
                $b->queuecmd(cmd(102,2,chr(4).chr(0)), 0, 0);
                #queuecmd(cmd(102,2,chr(4).chr(0)), 0, 0);
                #print "FIRE:". ($s->{'data'}{'target'}[1])."\n";
            }
        } elsif($b->{'buttons'}{'fire'}) {
            $b->queuecmd(cmd(102,2,chr(4).chr(1)), 0, 0);
        }
    }
    $s->{'data'}{'tick'}++;
}

sub research {
    my($s, $b)=@_;

    if(
        !@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{4}} || #No labs
        $b->{'players'}{$b->{'self'}{'id'}}{'research'}!=255 || #Researching (WARNING, what if researching and labs attacked?)
        $s->{'data'}{'upgrade'}{'m'} #All done? (Use switch later)
    ) { return; }

    my $skipped=0;
    for(my $i=0; $i<@{$s->{'data'}{'upgrade'}{'up'}}; $i++) {
        my $type=$s->{'data'}{'upgrade'}{'up'}[$i];
        if($b->{'players'}{$b->{'self'}{'id'}}{'upgrades'}[$type]==$upgrade{$type}{'max'}) { next; }
        if($b->{'players'}{$b->{'self'}{'id'}}{'res'}<$upgrade{$type}{'cost'}) { $skipped=1; next; }
        print "Research: ".$upgrade{$type}{'name'}."\n";
        $b->queuecmd($b->research($type), 0, 1);
        return;
    }
    if($skipped) { return; }
    print "All research done\n";
    $s->{'data'}{'upgrade'}{'m'}=1;
}

sub farm {
    my($s, $b)=@_;

    switch($s->{'data'}{'farm'}{'m'}) {
        case 0 {
            my $angle=pi*2*rand();
            my $dist=rand()*5+10;
            $s->{'data'}{'farm'}{'pos'}=[cos($angle)*3, sin($angle)*3]; #Set farm location
            $s->{'data'}{'farm'}{'m'}=1;
        }
        case 1 {
            $b->queuecmd($b->buildx(3, $s->{'data'}{'farm'}{'pos'}[0], $s->{'data'}{'farm'}{'pos'}[1]), 0, 1);
            $s->{'data'}{'farm'}{'m'}=2;
            $s->{'data'}{'farm'}{'cntr'}=0;
        }
        case 2 {
            if($s->{'data'}{'farm'}{'cntr'}++>50) { $s->{'data'}{'farm'}{'m'}=1; return; }# Retry
            if(!@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{3}}) { return; } #None yet

            for(my $i=0; $i<@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{3}}; $i++) {
                if(distb($b->{'entities'}{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{3}[$i]}, $s->{'data'}{'farm'}{'pos'}[0], $s->{'data'}{'farm'}{'pos'}[1])>0.1**2) { return; }

                $s->{'data'}{'farm'}{'power'}=$i;
                $s->{'data'}{'farm'}{'m'}=3;
                last;
            }
        }
        case 3 {
            my $t=0;
            for(my $i=0; $i<@{$s->{'data'}{'farm'}{'build'}}; $i++) { #Make farm
                my $type=$s->{'data'}{'farm'}{'build'}[$i];
                my $cnt=@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{$type}};
                while(1) {
                    if($b->{'players'}{$b->{'self'}{'id'}}{'res'}<$entity{$type}{'cost'}) { return; } #Too poor
                    if($cnt==$entity{$type}{'max'}) { last; } #Not maxed out

                    my $angle=(pi*2/$entity{$type}{'max'})*$cnt;
                    my $dist=$type*0.1-0.1;
                    $b->queuecmd($b->build($type, $s->{'data'}{'farm'}{'pos'}[0]+cos($angle)*$dist, $s->{'data'}{'farm'}{'pos'}[1]+sin($angle)*$dist, 0), 0, 1);
                    $cnt++;
                }
                if($cnt!=@{$b->{'players'}{$b->{'self'}{'id'}}{'ents'}{$type}}) { $t=1; }
            }
            if($t) { return; } #Gotta check if they're built
            $s->{'data'}{'farm'}{'m'}=4;
        }
        case 4 { #Monitor mode?

        }
        case 9 { #Enemy has found farm!


        }
    }
}
=cut
1;