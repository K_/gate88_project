package Gate88::Bot::StateStack;

use Modern::Perl;

use Scalar::Util qw/refaddr/;

sub new {
    my $class = shift;
    my($bot) = @_;

    return bless({
        'states' => [],
        'bot' => $bot,
        'event_map' => {},
        'timer_map' => {},
    }, $class);
}

sub proc {
    my($s) = @_;

    my $state = $s->top();
    if($state) {
        my $ret = $state->proc($s->{'bot'});

        if(!$ret) {
            $s->pop();
        }
    }
}

sub top {
    my($s) = @_;

    return $s->{'states'}[-1];
}

sub push {
    my($s, $state) = @_;

    my $old_state = $s->top();
    CORE::push(@{$s->{'states'}}, $state);

	#Same as below
    #if($old_state) {
    #    $old_state->leave($s->{'bot'});
    #}
    Gate88::Core::Logger::log("Push-> " . $state, 4);
    $s->_init_handlers($state);
    $state->init($s->{'bot'});
	#I think enter() should be called only on ss->pop() and not on ss->push()
	#Because we may push more states after it and don't want to enter this state immediately
    #$state->enter($s->{'bot'});
}

sub pop {
    my($s) = @_;

    my $old_state = CORE::pop(@{$s->{'states'}});
    my $state = $s->top();

    if(!$old_state) {
		Gate88::Core::Logger::log("ss->Pop: Error: No state to pop!", 0);
        return;
    }

    $old_state->leave($s->{'bot'});
    $old_state->destroy($s->{'bot'});
    $s->_clear_handlers($old_state);
    if($state) {
        Gate88::Core::Logger::log("Pop-> " . $state, 4);
        $state->enter($s->{'bot'});
    }
}

sub replace {
    my($s, $state) = @_;

    my $old_state = CORE::pop(@{$s->{'state'}});
    CORE::push(@{$s->{'states'}}, $state);

    if($old_state) {
        $old_state->leave($s->{'bot'});
        $old_state->destroy($s->{'bot'});
        $s->_clear_handlers($old_state);
    }
    Gate88::Core::Logger::log("Replace-> " . $state, 4);
    $s->_init_handlers($state);
    $state->init($s->{'bot'});
    $state->enter($s->{'bot'});
}

sub clear {
    my($s, $force) = @_;

    while($s->top()) {
        if(!$force) {
            $s->pop();
        } else {
            CORE::pop(@{$s->{'state'}});
        }
    }
    Gate88::Core::Logger::log("Clear States", 4);
}

sub _init_handlers {
    my($s, $state) = @_;

    my $id = refaddr($state);

    $s->{'event_map'}{$id} = [];
    $s->{'timer_map'}{$id} = [];
}

sub _clear_handlers {
    my($s, $state) = @_;

    my $e = $s->{'event_map'};
    my $t = $s->{'timer_map'};
    my $id = refaddr($state);

    foreach my $callback (@{$e->{$id}}) {
        $s->{'bot'}{'nm'}->remcallback(@$callback);
    }
    delete $e->{$id};

    foreach my $callback (@{$t->{$id}}) {
        $s->{'bot'}{'tm'}->remcallback(@$callback);
    }
    delete $e->{$id};
}

sub add_handler {
    my($s, $state, $type, $callback) = @_;

    my $e = $s->{'event_map'};
    my $id = refaddr($state);
    my $arr = [$type, sub { my($nm, $cmd) = @_; &$callback($state, $cmd); }];

    $s->{'bot'}{'nm'}->addcallback(@$arr);
    CORE::push(@{$e->{$id}}, $arr);
}

sub remove_handler {
    my($s, $state, $type, $callback) = @_;

    my $id = refaddr($state);
    my $e = $s->{'event_map'};
    my $arr = [$type, $callback];

    $s->{'bot'}{'nm'}->remcallback(@$arr);

    for(my $i = 0; $i < @{$e->{$id}}; ++$i) {
        if($e->{$id}[$i][0] == $arr->[0] && $e->{$id}[$i][1] == $arr->[1]) {
            splice(@{$e->{$id}}, $i, 1);
            --$i;
        }
    }
}

sub add_timer {
    my($s, $state, $delay, $callback) = @_;

    my $t = $s->{'timer_map'};
    my $id = refaddr($state);
    my $arr = [$delay, sub { &$callback($state); }];

    $s->{'bot'}{'tm'}->addcallback(@$arr);
    CORE::push(@{$t->{$id}}, $arr);
}

sub remove_timer {
    my($s, $state, $delay, $callback) = @_;

    my $id = refaddr($state);
    my $t = $s->{'timer_map'};
    my $arr = [$delay, $callback];

    $s->{'bot'}{'tm'}->remcallback(@$arr);

    for(my $i = 0; $i < @{$t->{$id}}; ++$i) {
        if($t->{$id}[$i][0] == $arr->[0] && $t->{$id}[$i][1] == $arr->[1]) {
            splice(@{$t->{$id}}, $i, 1);
            --$i;
        }
    }
}
1;
