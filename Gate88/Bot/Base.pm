package Gate88::Bot::Base;
use parent qw/Gate88::Bot/;

use Modern::Perl;

use Gate88::Bot::StateStack;

sub new {
    my $class = shift;
    my($name) = @_;

    my $self = $class->SUPER::new($name);
    $self->{'ss'} = Gate88::Bot::StateStack->new($self);
    $self->{'mgr'} = {};

    $self->addintvcallback(0.1, \&proc_state);

    return $self;
}

sub proc_state {
    my($s) = @_;

    $s->{'ss'}->proc();
}

sub add_manager {
    my($s, $n, $mgr) = @_;

    $s->{'mgr'}{$n} = $mgr;
}

1;
