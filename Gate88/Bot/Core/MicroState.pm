package Gate88::Bot::Core::MicroState;

use warnings;
use strict;

use Gate88::Bot::Core::Timer;

use constant {
	'TRUE' => 1,
	'FALSE' => 0,
	'USTATE_WAITING' => 0,
};

#MicroStates are polled states inside a Gate88::Bot::State object
#They make polling much easier
#Inside the State object, there is a Microstate (ustate), which is represented by an integer
#0 is reserved for the waiting state (happens if next state is delayed)
sub new {
    my $class = shift;
    return bless({
        'timer' => Gate88::Bot::Core::Timer->new(),
		'cur_ustate' => 1,
		'next_ustate' => 2,
		'last_ustate' => 1,
		'once' => FALSE,
		'init_timer' => TRUE,
    }, $class);
}

#Get current ustate
sub get_active {
	my $s = shift;
	if($s->{'init_timer'}) {
		$s->{'timer'}->start();
		$s->{'init_timer'} = FALSE;
	}
	if( $s->{'cur_ustate'} == USTATE_WAITING ) {		#waiting
		if( $s->{'timer'}->check() ) {					#set next ustate
			$s->{'cur_ustate'} = $s->{'next_ustate'};
			$s->{'once'} = FALSE;
			$s->{'timer'}->start();
		}
	}
	return $s->{'cur_ustate'};
}

#Set next ustate immediately
sub next {
	my $s = shift;
	$s->{'last_ustate'} = $s->{'cur_ustate'};
	$s->{'cur_ustate'}++;
	$s->{'next_ustate'}++;
	$s->{'once'} = FALSE;
	$s->{'timer'}->start();
}

#Set next ustate with delay, USTATE_WAITING will be inbetween
sub next_delayed {
	my $s = shift;
	my $delay_time = shift;		#in seconds
	$s->{'timer'}->set($delay_time);
	$s->{'last_ustate'} = $s->{'cur_ustate'};
	$s->{'next_ustate'} = $s->{'cur_ustate'}+1;
	$s->{'cur_ustate'} = USTATE_WAITING;		#waiting
}

sub restart() {
	my $s = shift;
	$s->{'last_ustate'} = $s->{'cur_ustate'};
	$s->{'next_ustate'} = $s->{'cur_ustate'};
	$s->{'once'} = FALSE;
	$s->{'timer'}->start();
}

#Restart this ustate but take a delay first
sub restart_delayed {
	my $s = shift;
	my $delay_time = shift;		#in seconds
	$s->{'timer'}->set($delay_time);
	$s->{'last_ustate'} = $s->{'cur_ustate'};
	$s->{'next_ustate'} = $s->{'cur_ustate'};
	$s->{'cur_ustate'} = USTATE_WAITING;		#waiting
}

sub previous {
	my $s = shift;
	if( $s->{'cur_ustate'} > 1 ) {
		$s->{'last_ustate'} = $s->{'cur_ustate'};
		$s->{'cur_ustate'}--;
		$s->{'next_ustate'}--;
		$s->{'once'} = FALSE;
		$s->{'timer'}->start();
	}
}

#Poll this for any task you want to do only once within a ustate
sub once {
	my $s = shift;
	if( !$s->{'once'} ) {
		$s->{'once'} = TRUE;
		return TRUE;
	} else {
		return FALSE;
	}
}

#When the object is in waiting state, get the last active state (not 0)
sub get_last {
	my $s = shift;
	return $s->{'last_ustate'};
}

#Check if time passed as parameter has already elapsed since the start of current ustate
sub check_elapsed {
	my $s = shift;
	my $intv = shift;	#interval time in seconds
	if( $intv > $s->{'timer'}->time() ) {
		return FALSE;
	} else {
		return TRUE;
	}
}

=cut
#Example code/testing
use feature qw(switch);

given($s->{'ustate'}->get_active()){
	when(1) {
		if( $s->{'ustate'}->once() ) {
			print "1111111111111111111\n";
		}
		print "Number one\n"; 
		if ($s->{'ustate'}->check_elapsed(1.0)) {
			$s->{'ustate'}->next();
		}
	}
	when(2) {
		if( $s->{'ustate'}->once() ) {
			print "222222222222222222222\n";
		}
		print "Number two\n";
		if ($s->{'ustate'}->check_elapsed(1.0)) {
			$s->{'ustate'}->previous();
		}
	}
	default { print "Error\n"; }
}

#Another one
given($s->{'ustate'}->get_active()){
	when(0) {
		print "Waiting\n";
	}
	when(1) {
		if( $s->{'ustate'}->once() ) {
			print "1111111111111111111\n";
		}
		print "Number one\n"; 
		if ($s->{'ustate'}->check_elapsed(2.0)) {
			$s->{'ustate'}->next_delayed(1.0);
		}

	}
	when(2) {
		if( $s->{'ustate'}->once() ) {
			print "222222222222222222222\n";
		}
		print "Number two\n";
		if ($s->{'ustate'}->check_elapsed(2.0)) {
			$s->{'ustate'}->next_delayed(1.0);
		}
	} when(3) {
		print "3\n";
	}
	default { print "Error\n"; }
}

=cut
1;