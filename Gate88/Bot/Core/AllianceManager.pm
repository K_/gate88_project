package Gate88::Bot::Core::AllianceManager;

use Modern::Perl;
use Gate88::Core::Util::CommandBuilder;

sub new {
    my $class = shift;
    my($bot) = @_;

    my $self = bless({}, $class);
    $self->{'bot'} = $bot;
    $self->reset();

    $bot->{'ss'}->add_timer($self, 1.0, \&_check_alliances);
    #Let's only process every second
    #$bot->{'ss'}->add_handler($self, Gate88::Core::Command::AllianceStatusNotify::TYPE, \&_check_alliances);
    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::PlayerJoinNotify::TYPE, \&_playerjoin_n);
    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::PlayerLeaveNotify::TYPE, \&_playerleave_n);

    return $self;
}

sub reset {
    my($s) = @_;

    $s->{'amap'} = {};

    foreach my $id (keys(%{$s->{'bot'}->{'gs'}{'players'}})) {
        $s->{'amap'}{$id} = 0;
    }
}

sub _check_alliances {
    my($s) = @_;

    foreach my $id (keys(%{$s->{'amap'}})) {
        my $player = $s->{'bot'}{'gs'}{'players'}{$id};
        if($player->{'ally'} != $s->{'amap'}{$id}) {
            $s->_toggle($id);
        }
    }
}

sub _playerleave_n {
    my($s, $cmd, $src) = @_;

    my $id = $cmd->player_id;
    if(defined($s->{'amap'}{$id})) {
        delete $s->{'amap'}{$id};
    }
}

sub _playerjoin_n {
    my($s, $cmd, $src) = @_;
    my $id = $cmd->player_id;

    if(!defined($s->{'amap'}{$id})) {
        $s->{'amap'}{$id} = 0;
    }
}

sub ally {
    my($s, $id) = @_;

    $s->{'amap'}{$id} = 1;
}

sub unally {
    my($s, $id) = @_;

    $s->{'amap'}{$id} = 0;
}

sub _toggle {
    my($s, $id) = @_;

    $s->{'bot'}->queuecmdserver(Gate88::Core::Util::CommandBuilder::AllianceRequestNotify(
        $s->{'bot'}{'nm'}->g_counter(), $id,
        $s->{'bot'}{'nm'}->counter(Gate88::Core::Command::AllianceRequestNotify::TYPE)
    ));
}

1;
