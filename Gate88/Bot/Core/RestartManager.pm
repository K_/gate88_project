package Gate88::Bot::Core::RestartManager;

use Modern::Perl;

sub new {
    my $class = shift;
    my($bot) = @_;

    my $self = bless({}, $class);
    $self->{'bot'} = $bot;

    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::RoundStartNotify::TYPE, \&_roundstart_n);

    return $self;
}

sub _roundstart_n {
    my($s, $id) = @_;

    $s->{'bot'}{'ss'}->clear();
    $s->{'bot'}{'ss'}->push(Gate88::Bot::State::SetupState->new());

    $s->{'bot'}{'mgr'}{'am'}->reset();
    $s->{'bot'}{'mgr'}{'bm'}->reset();
    $s->{'bot'}{'mgr'}{'km'}->reset();
    $s->{'bot'}{'mgr'}{'lm'}->reset();
}

1;
