package Gate88::Bot::Core::KeyManager;

use Modern::Perl;

use Gate88::Core;

sub new {
    my $class = shift;
    my($bot) = @_;

    my $self = bless({}, $class);
    $self->{'bot'} = $bot;
    $self->reset();

    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::ShipStatusNotify::TYPE, \&_shipstatus_n);

    return $self;
}

sub reset {
    my($s) = @_;

    $s->{'klist'} = [];
    $s->{'kmap'} = [];
    foreach(0..5) {
        push($s->{'kmap'}, Gate88::Core::Key::STATE_UP);
    }
}

sub _shipstatus_n {
    my($s, $cmd) = @_;

    foreach my $i (0..5) {
        my $state = $s->{'bot'}{'gs'}{'self'}{'keys'}[$i];
        my $new_state = $s->{'kmap'}[$i];
        if($state == $s->{'kmap'}[$i]) {
            next;
        }

        if($state == Gate88::Core::Key::STATE_DOUBLE &&
            $new_state == Gate88::Core::Key::STATE_DOWN
        ) {
            $new_state = Gate88::Core::Key::STATE_UP;
        }

        $s->_setkey($i, $new_state);
    }
}

sub _setkey {
    my($s, $key, $state) = @_;

    $s->{'bot'}->queuecmdserver(Gate88::Core::Util::CommandBuilder::KeyUpdate(
        $key, $state
    ));
}

sub idle {
    my($s) = @_;

    foreach my $i (0..5) {
        $s->{'kmap'}[$i] = Gate88::Core::Key::STATE_UP;
    }
}

sub set {
    my($s, $key, $state) = @_;

    # check for lr, ud
    $s->{'kmap'}[$key] = $state;
}

sub left {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::LEFT] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::RIGHT] = Gate88::Core::Key::STATE_UP;
}

sub right {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::RIGHT] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::LEFT] = Gate88::Core::Key::STATE_UP;
}

sub neutral {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::LEFT] = Gate88::Core::Key::STATE_UP;
    $s->{'kmap'}[Gate88::Core::Key::RIGHT] = Gate88::Core::Key::STATE_UP;
}

sub drift {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::UP] = Gate88::Core::Key::STATE_UP;
    $s->{'kmap'}[Gate88::Core::Key::DOWN] = Gate88::Core::Key::STATE_UP;
}

sub brake {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::UP] = Gate88::Core::Key::STATE_UP;
    $s->{'kmap'}[Gate88::Core::Key::DOWN] = Gate88::Core::Key::STATE_DOUBLE;
}

sub forward {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::UP] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::DOWN] = Gate88::Core::Key::STATE_UP;
}

sub backward {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::DOWN] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::UP] = Gate88::Core::Key::STATE_UP;
}

sub fire {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::SHOOT] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::SPECIAL] = Gate88::Core::Key::STATE_UP;
}

sub special {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::SPECIAL] = Gate88::Core::Key::STATE_DOWN;
    $s->{'kmap'}[Gate88::Core::Key::SHOOT] = Gate88::Core::Key::STATE_UP;

}

sub ceasefire {
    my($s) = @_;

    $s->{'kmap'}[Gate88::Core::Key::SHOOT] = Gate88::Core::Key::STATE_UP;
    $s->{'kmap'}[Gate88::Core::Key::SPECIAL] = Gate88::Core::Key::STATE_UP;
}

1;
