package Gate88::Bot::Core::LocationManager;

use Modern::Perl;
use Gate88::Core::Util::Function;

use constant {
    'TELEPORT' => 1,
    'STRAIGHT' => 2,
    'STEALTH' => 3
};

sub new {
    my $class = shift;
    my($bot) = @_;

    my $self = bless({}, $class);
    $self->{'bot'} = $bot;
    $self->reset();

    return $self;
}

sub reset {
    my($s) = @_;

    $s->{'llist'} = [];
}

sub proc {
    my($s, $bot) = @_;

    if(@{$s->{'llist'}} == 0) {
        return;
    }

    my($type, $target, $data) = @{$s->{'llist'}[0]};
    my $pos = $s->_project($bot->{'gs'}{'self'}{'ship'}, 0.01);
    if(ref($target) eq 'ARRAY') {
        my $entity = $bot->{'gs'}{'ents'}->get($target);
        $target = $s->_project($entity, 0.01);
    }

    my $ret;
    given($type) {
        when(TELEPORT) { $ret = $s->_teleport($bot, $pos, $target, $data); }
        when(STRAIGHT) { $ret = $s->_straight($bot, $pos, $target, $data); }
        when(STEALTH)  { $ret = $s->_stealth ($bot, $pos, $target, $data); }
    }

    if($ret == 0) {
        shift($s->{'llist'});
    }
}

sub clear {
    my($s) = @_;

    #TODO: Should clean up!
    $s->{'llist'} = [];
}

sub _teleport {
    my($s, $bot, $pos, $target, $data) = @_;

    if(!$data->{'built'}) {
        #$bot->{'mgrs'}{'bm'}->build();
        $data->{'built'} = 1;
    }

    return 1;
}

sub _straight {
    my($s, $bot, $pos, $target, $data) = @_;
    my $ship = $bot->{'gs'}{'self'}{'ship'};

    my $angle = atan2($target->[1] - $pos->[1], $target->[0] - $pos->[0]);
    my $dist = dist($pos->[0], $target->[0], $pos->[1], $target->[1]);

    #Aim, but only if angle is incorrect
    if($angle != $ship->angle) {
        $bot->queuecmdserver(Gate88::Core::Util::CommandBuilder::ShipUpdate(
            cos($angle), sin($angle), $ship->x, $ship->y, 0, 0
        ));
    }

    #If the ship is close...
    if($dist < $ship->SIZE + $s->{'dist'}) {
        #Check if it's too close...
        if($dist < $ship->SIZE + $s->{'dist'} - 0.2) {
            $bot->{'mgrs'}{'km'}->backward();
            print "2Close\n";
        } else {
            print "SLOW\n";
            #Slow down
            if( abs($ship->x_velocity) > Gate88::Core::Util::Bits::EPSILON &&
                abs($ship->y_velocity) > Gate88::Core::Util::Bits::EPSILON
            ) {
                $bot->{'mgrs'}{'km'}->brake();
            } else {
                $bot->{'mgrs'}{'km'}->drift();
                return 0;
            }
        }
    #Still far away
    } else {
        $bot->{'mgrs'}{'km'}->forward();
    }

    return 1;
}

sub _stealth {
    my($s, $bot, $pos, $target) = @_;

    return 1;
}

sub _project {
    my($s, $ent, $t) = @_;

    return [$ent->x + $ent->vx * $t, $ent->y + $ent->vy * $t];
}

sub add {
    my($s, $type, $location, $preemptive) = @_;

    my $data = [$type, $location, {}];
    if($preemptive) {
        unshift($s->{'llist'}, $data);
    } else {
        push($s->{'llist'}, $data);
    }
}

sub teleport {
    my($s, $location, $preemptive) = @_;

    $s->add(TELEPORT, $location, $preemptive);
}

sub straight {
    my($s, $location, $preemptive) = @_;

    $s->add(STRAIGHT, $location, $preemptive);
}

sub stealth {
    my($s, $location, $preemptive) = @_;

    $s->add(STEALTH, $location, $preemptive);
}

1;
