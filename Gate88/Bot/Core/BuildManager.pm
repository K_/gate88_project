package Gate88::Bot::Core::BuildManager;

use Modern::Perl;
use Gate88::Core::Entity;

sub new {
    my $class = shift;
    my($bot) = @_;

    my $self = bless({}, $class);
    $self->{'bot'} = $bot;
    $self->reset();

    $bot->{'ss'}->add_timer($self, 1.0, \&_check_builds);

    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::EntityCreateNotify::TYPE, \&_entcreate_n);
    $bot->{'ss'}->add_handler($self, Gate88::Core::Command::EntityDeleteNotify::TYPE, \&_entdelete_n);

    return $self;
}

sub reset {
    my($s) = @_;

    $s->{'dlist'} = [];
    $s->{'blist'} = [];
}

sub _check_builds {
    my($s) = @_;

    if(!$s->{'bot'}{'gs'}{'self'}{'base'}) {
        return;
    }

    foreach my $entity (@{$s->{'blist'}}) {
        my $cost = $Gate88::Core::Entity::TABLE[$entity->[0]]->COST;
        #FIXME: Callback if 2poor?
        if($cost > $s->{'bot'}{'gs'}{'self'}{'resources'}) {
            last;
        }

        $s->_build(@$entity);
    }

    foreach my $id (@{$s->{'dlist'}}) {
        $s->_destroy($id);
    }
}

sub _entcreate_n {
    my($s, $cmd) = @_;

    if($cmd->player_id != $s->{'bot'}{'gs'}{'self'}{'id'}) {
        return;
    }

    my $list = $s->{'blist'};
    for(my $i = 0; $i < @$list; ++$i) {
        if(
            $list->[$i][0] == $cmd->entity_type &&
            ($list->[$i][1] - $cmd->x) < Gate88::Core::Util::Bits::EPSILON &&
            ($list->[$i][2] - $cmd->y) < Gate88::Core::Util::Bits::EPSILON
        ) {
            splice(@$list, $i, 1);
            last;
        }
    }
}

sub _build {
    my($s, $type, $x, $y) = @_;

    $s->{'bot'}->queuecmdserver(Gate88::Core::Util::CommandBuilder::EntityCreate(
        $s->{'bot'}{'nm'}->g_counter(), $type, $x, $y,
        $s->{'bot'}{'nm'}->counter(Gate88::Core::Command::EntityCreate::TYPE)
    ));
}

sub _destroy {
    my($s, $id) = @_;

    my $entity = $s->{'bot'}{'gs'}{'entities'}->get($id);

    $s->{'bot'}->queuecmdserver(Gate88::Core::Util::CommandBuilder::EntityDelete(
        $s->{'bot'}{'nm'}->g_counter(),
        $s->{'bot'}{'nm'}->counter(Gate88::Core::Command::EntityDelete::TYPE),
        $entity->x, $entity->y - 0.005, 0, 1
    ));
}

sub _entdelete_n {
    my($s, $cmd) = @_;

    my $list = $s->{'dlist'};
    for(my $i = 0; $i < @$list; ++$i) {
        if($cmd->entity_id == $list->[$i]) {
            splice(@$list, $i, 1);
            last;
        }
    }
}

sub build {
    my($s, $type, $x, $y) = @_;

    push(@{$s->{'blist'}}, [$type, $x, $y]);
}

sub destroy {
    my($s, $id) = @_;

    push(@{$s->{'dlist'}}, $id);
}

1;
