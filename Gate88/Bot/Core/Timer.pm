package Gate88::Bot::Core::Timer;

use warnings;
use strict;

use Time::HiRes qw( gettimeofday );

sub new {
    my $class = shift;

    my $self = bless(
        {
            #don't use these directly
            'start_sec' => 0,   #When was the timer started, in seconds
                                #It contains microseconds too in fractional part
            'expr_sec'  => 0,   #When does the timer expire from start
        },
        $class
    );

    return $self;
}

sub start {
    my $s = shift;
    $s->{'start_sec'} = gettimeofday;
}

#Get elapsed time in seconds after start()
sub time {
    my $s       = shift;
    my $cur_sec = gettimeofday;
    return ( $cur_sec - $s->{'start_sec'} );
}

#Parameter: expiration time from start, in seconds (eg. 0.01 for 10 ms )
sub set {
    my ( $s, $time_sec ) = @_;
    $s->{'start_sec'} = gettimeofday;
    $s->{'expr_sec'}  = $time_sec;
}

#Check if the timer has expired
sub check {
    my $s       = shift;
    my $cur_sec = gettimeofday;
    return ( $cur_sec >= ( $s->{'start_sec'} + $s->{'expr_sec'} ) );
}

1;
