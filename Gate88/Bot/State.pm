package Gate88::Bot::State;

use Modern::Perl;

sub new {
    my $class = shift;

    return bless({}, $class);
}

sub init {}

sub enter {}

sub proc {
    return 1;
}

sub event {}

sub leave {}

sub destroy {}

1;