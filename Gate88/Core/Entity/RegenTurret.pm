package Gate88::Core::Entity::RegenTurret;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    FIELDS => ['x', 'y', 'x_velocity', 'y_velocity', 'hp', 'angle', 'cloak', 'cooldown'],

    NAME => 'Regen Turret',
    TYPE => 0x0e,
    COST => 50,
    MAX => 10,
    SIZE => 0.05,
#    HP =>
    MASS => 6.0,
    SPEED => 0
};
1;