package Gate88::Core::Entity::JumpGate;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Jump Gate',
    TYPE => 0x08,
    COST => 30,
    MAX => 20,
#    SIZE => 0.1, #Might be wrong
#    HP =>
    MASS => 0.0,
    SPEED => 0
};
1;