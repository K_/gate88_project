package Gate88::Core::Entity::ExciterTurret;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    FIELDS => ['x', 'y', 'x_velocity', 'y_velocity', 'hp', 'angle', 'cloak', 'cooldown'],

    NAME => 'Exciter Turret',
    TYPE => 0x0f,
    COST => 50,
    MAX => 15,
    SIZE => 0.0325,
#    HP =>
    MASS => 6.0,
    SPEED => 0
};
1;