package Gate88::Core::Entity::FighterBase;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Fighter Base',
    TYPE => 0x14,
    COST => 100,
    MAX => 6,
    SIZE => 0.125,
#    HP =>
    MASS => 400.0,
    SPEED => 0
};
1;