package Gate88::Core::Entity::SignalStation;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Signal Station',
    TYPE => 0x07,
    COST => 15,
    MAX => 10,
    SIZE => 0.075,
#    HP =>
    MASS => 200.0,
    SPEED => 0
};
1;