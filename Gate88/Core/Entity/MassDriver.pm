package Gate88::Core::Entity::MassDriver;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    FIELDS => ['x', 'y', 'x_velocity', 'y_velocity', 'hp', 'angle', 'cloak', 'cooldown'],

    NAME => 'Mass Driver',
    TYPE => 0x0b,
    COST => 50,
    MAX => 10,
    SIZE => 0.075,
#    HP =>
    MASS => 500.0,
    SPEED => 0
};
1;