package Gate88::Core::Entity::BomberBase;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Bomber Base',
    TYPE => 0x15,
    COST => 150,
    MAX => 2,
    SIZE => 0.125,
#    HP =>
    MASS => 400,
    SPEED => 0
};
1;