package Gate88::Core::Entity::CommandPost;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Command Post',
    TYPE => 0x02,
    COST => 0,
    MAX => 1,
    SIZE => 0.225,
#    HP =>
    MASS => 2000,
    SPEED => 0
};
1;