package Gate88::Core::Entity::Factory;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Factory',
    TYPE => 0x05,
    COST => 25,
    MAX => 12,
    SIZE => 0.05,
#    HP =>
    MASS => 5.0,
    SPEED => 0
};
1;