package Gate88::Core::Entity::Bomb;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    NAME => 'Bomb',
    TYPE => 0x06,
    COST => 20,
    MAX => 10000,
    SIZE => 0.03,
#    HP =>
    MASS => 1.0,
    SPEED => 0
};
1;