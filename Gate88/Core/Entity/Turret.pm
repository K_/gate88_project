package Gate88::Core::Entity::Turret;
use parent Gate88::Core::Entity;

use Modern::Perl;

use constant {
    FIELDS => ['x', 'y', 'x_velocity', 'y_velocity', 'hp', 'angle', 'cloak', 'cooldown'],

    NAME => 'Turret',
    TYPE => 0x0a,
    COST => 15,
    MAX => 30,
    SIZE => 0.0375,
#    HP =>
    MASS => 5.0,
    SPEED => 0
};
1;