package Gate88::Core::Util::CommandBuilder;

use Modern::Perl;

use IO::Socket::INET;

our $AUTOLOAD;

my $base = 'Gate88::Core::Command::';

sub AUTOLOAD {
    my $module = $base . (split('::', $AUTOLOAD))[-1];

    return $module->new(@_);
}

sub CommandAcknowledge {
    my($cmd) = @_;

    my $type = $cmd->TYPE;
    my $g_counter = $cmd->g_counter;
    return Gate88::Core::Command::CommandAcknowledge->new($type, $g_counter);
}

sub ServerListSend {
    my($g_counter, $servers, $page) = @_;

    my $start = $page*198;
    my $end = ($page+1)*198;
    my @list = (keys(%$servers))[$start..$end];

    my $count = 0;
    my $buf = '';
    for my $glob (@list) {
        if(defined($glob)) {
            my($port, $ip) = sockaddr_in($glob);
            my $server = pack('a[4]S>', $ip, $port);
            $buf .= $server;
            ++$count;
        }
    }

    return Gate88::Core::Command::ServerListSend->new($g_counter, $page, $count, $buf);
}

sub ServerConnect {
    my($counter, $name) = @_;

    return Gate88::Core::Command::ServerConnect->new($counter, 13, 16000, $name);
}

sub GameAckCounterStart {
    my($g_counter) = @_;

    return Gate88::Core::Command::GameAckCounterStart->new($g_counter, 0);
}

sub ServerInfoSend {
    my($counter, $players, $max_players, $time, $name) = @_;

    return Gate88::Core::Command::ServerInfoSend->new(
        $counter, $players, $max_players, $time, $name, 13
    );
}

sub ServerRegistrationPing {
    my($counter, $port) = @_;

    return Gate88::Core::Command::ServerRegistrationPing->new(
        $counter, 0, $port
    );
}

sub BaseCreate {
    my($g_counter, $x, $y) = @_;

    return Gate88::Core::Command::BaseCreate->new($g_counter,
        $x, $y - Gate88::Core::Entity::PlayerShip::SIZE - Gate88::Core::Entity::CommandPost::SIZE,
        0, 1
    );
}

sub EntityCreate {
    my($g_counter, $type, $x, $y, $counter) = @_;

    my $module = $Gate88::Core::Entity::TABLE[$type];

    return Gate88::Core::Command::EntityCreate->new($g_counter, $type,
        $x, $y - Gate88::Core::Entity::PlayerShip::SIZE - $module->SIZE - 0.005,
        0, 1, $counter
    );
}
1;
