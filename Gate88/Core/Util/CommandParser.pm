package Gate88::Core::Util::CommandParser;

use Modern::Perl;

sub parse {
    my($raw) = @_;

    my @ret;
    my $offset = 0;
    while($offset <= length($raw) - 3) {
        my $type = ord(substr($raw, $offset, 1));

        if(defined($Gate88::Core::Command::TABLE[$type])) {
            my $module = $Gate88::Core::Command::TABLE[$type];
            my $size = $module->SIZE;
            my $data = substr($raw, $offset, $size);

            my $cmd = $module->new();
            $cmd->initialize($data);

            push(@ret, $cmd);
            $offset += $size;
        } else {
            #DEBUG
            print "Unknown packet [$type]\n";
            my $str = '';
            map { $str .= sprintf "%02x:", ord } (split('', substr($raw, $offset)));
            open(my $fh, '>>', 'Unknown.log');
            print $fh "$type " . unpack('S>', substr($raw, $offset + 1, 2)) . " $str\n";
            close($fh);
            last;
        }
    }
    if($offset != length($raw)) {
        print "Incomplete parsing\n";
    }
    return @ret;
}
1;