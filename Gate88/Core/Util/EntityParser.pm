package Gate88::Core::Util::EntityParser;

use Modern::Perl;

sub parse {
    my($cmd) = @_;

    my $ret = undef;
    my $type = $cmd->entity_type;

    if(defined($Gate88::Core::Entity::TABLE[$type])) {
        my $module = $Gate88::Core::Entity::TABLE[$type];
        $ret = $module->new();

        $ret->initialize($cmd);
    } else {
        print "Unknown entity [$type]\n";
    }
    return $ret;
}
1;