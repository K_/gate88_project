package Gate88::Core::Util::Function;

use Modern::Perl;

use Math::Vector::Real;
use Math::Trig ':pi';

use Exporter 'import';
our @EXPORT = ('hexdump', 'clamp', 'dist', 'dist_sq', 'get_length', 'get_ang_diff');

sub hexdump {
    my($data) = @_;
    map { printf "%02x:", ord } (split('', $data));
    print "\n";
}

sub clamp {
    my($a, $b, $c) = @_;
    return $a > $b ? $a : $c < $b ? $c : $b;
}

#Return the sq dist between two points
sub dist_sq {
    my($ax, $ay, $bx, $by) = @_;

    return ($bx - $ax) ** 2 + ($by - $ay) ** 2;
}

#Return the dist between two points
sub dist {
    my($ax, $ay, $bx, $by) = @_;

    return dist_sq($ax, $ay, $bx, $by) ** 0.5;
}

#Get the length of a 2D vector
sub get_length {
    my $v = shift;
    return (($v->[0]**2 + $v->[1]**2)**0.5);
}

#Pass a 2D vector as reference
#Creates unit vector from the passed vector
sub normalize {
    my($v_ref) = @_;
	my $len = ($v_ref->[0]**2 + $v_ref->[1]**2)**0.5;
	$$v_ref->[0] /= $len;
	$$v_ref->[1] /= $len;
}

#Angle difference between two angles in radians [-pi, +pi]
#Need this because angles wrap around at -pi/+pi
#Positive direction is counter clockwise
#Pointing from $ang1 to $ang2
sub get_ang_diff {
	my( $ang1, $ang2) = @_;
	my $ang_diff = $ang2 - $ang1;
	if ($ang_diff > pi) {
		$ang_diff = -2*pi+$ang_diff;
	} elsif($ang_diff < -1.0*pi) {
		$ang_diff = 2*pi+$ang_diff;
	}
	return $ang_diff;
}
1;
