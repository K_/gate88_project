package Gate88::Core::GameState;

use Modern::Perl;

sub new {
    my $class = shift;
    my($name) = @_;

    return bless({
        'time' => 0,
        'in_progress' => 0,

        'self' => Gate88::Core::Player->new($name),
        'players' => {},

        'entities' => Gate88::Core::EntityMap->new(),
    }, $class);
}

sub parsestart {
    my($s, $cmd) = @_;

    foreach my $id (keys(%{$s->{'players'}})) {
        $s->{'players'}{$id}->reset();
    }
    $s->{'entities'}->reset();
    $s->{'in_progress'} = 1;
}

sub parseend {
    my($s, $cmd) = @_;

    $s->{'in_progress'} = 0;
}

sub parsetime {
    my($s, $cmd) = @_;

    $s->{'time'} = $cmd->time/1000;
}
1;