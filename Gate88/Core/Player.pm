package Gate88::Core::Player;

use Modern::Perl;

use Gate88::Core::Key;

sub new {
    my $class = shift;
    my($name) = @_;

    my $self = bless({
        'id' => 0,
        'name' => ($name or ''),
        'ship' => undef,

        'max_resources' => 0,
        'latency' => 0
    }, $class);
    $self->reset();

    return $self;
}

#For new game
sub reset {
    my($s) = @_;

    $s->{'resources'} = 0;
    $s->{'upgrades'} = [];
    for(my $i = 0; $i < 10; ++$i) {
        push(@{$s->{'upgrades'}}, 0);
    }

    $s->{'keys'} = [];
    for(my $i = 0; $i < 10; ++$i) {
        push(@{$s->{'keys'}}, Gate88::Core::Key::STATE_UP);
    }

    $s->{'base'} = undef;

    $s->{'score'} = 0;
    $s->{'kills'} = 0;
    $s->{'deaths'} = 0;
    $s->{'cc_kills'} = 0;
    $s->{'cc_deaths'} = 0;

    $s->{'ally'} = 0;
}

sub initialize {
    my($s, $cmd) = @_;

    $s->{'id'} = $cmd->player_id;
    $s->{'name'} = $cmd->_name;
}

sub parsestatus {
    my($s, $cmd) = @_;

#FIXME: Not Done
    $s->{'resources'} = $cmd->resources;
    $s->{'max_resources'} = $cmd->max_resources;
}

sub parsescore {
    my($s, $cmd) = @_;

    $s->{'score'} = $cmd->score;
    $s->{'kills'} = $cmd->kills;
    $s->{'deaths'} = $cmd->deaths;
    $s->{'cc_kills'} = $cmd->cc_kills;
    $s->{'cc_deaths'} = $cmd->cc_deaths;
}

sub parseupgrade {
    my($s, $cmd) = @_;

    $s->{'upgrades'}[$cmd->upgrade] = $cmd->level;
}

sub parseally {
    my($s, $cmd) = @_;

    $s->{'ally'} = $cmd->status;
}
1;
