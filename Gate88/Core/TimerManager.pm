package Gate88::Core::TimerManager;

use Modern::Perl;

sub new {
    my $class = shift;

    return bless({
        'callbacks' => []
    }, $class);
}

sub proc {
    my($s, $intv, $gs, $nm) = @_;

    foreach my $c (@{$s->{'callbacks'}}) {
        $c->[2] += $intv;
        if($c->[2] > $c->[1]) {
            $c->[0]($s, $intv, $gs, $nm);
            $c->[2] = 0;
        }
    }
}

sub addcallback {
    my($s, $delay, $func) = @_;

    push(@{$s->{'callbacks'}}, [$func, $delay, 0])
}

sub remcallback {
    my($s, $delay, $func) = @_;

    my $c = $s->{'callbacks'};

    for(my $i = 0; $i < @$c; ++$i) {
        if($func == $c->[$i][0] && $delay == $c->[$i][1]) {
            splice(@$c, $i, 1);
            --$i;
        }
    }
}
1;
