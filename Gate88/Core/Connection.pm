package Gate88::Core::Connection;

use Modern::Perl;

use constant {
    NONE => 0,
    CLIENT => 1,
    SERVER => 2,
    MSERVER => 3
};

sub new {
    my $class = shift;
    my($ip, $port) = @_;

    return bless({
        'ip' => $ip,
        'port' => $port,
        'type' => NONE,
        'status' => 0,
        'queue' => [],
        'responsequeue' => [],
        'counter' => 0,
        'persistent' => 0,
    }, $class);
}
1;