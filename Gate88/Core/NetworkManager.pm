package Gate88::Core::NetworkManager;

use Modern::Perl;

use Gate88::Core;
use IO::Socket::INET;

sub new {
    my $class = shift;
    my($s) = @_;

    my $self = bless({
        's'=>$s,
        'connections' => {},
        'callbacks' => [],
        'counters' => [],
        'g_counter' => 0,

        'timeout_callback' => undef,

        'autoack' => 1,
        'autoresend' => 1,
    }, $class);

    return $self;
}

sub DESTROY {
    my($s) = @_;

    $s->{'s'}->close();
}

sub proc {
    my($s, $intv, $gs) = @_;

    $s->parsecmds($gs);
    $s->checkconns($intv);
}

sub _clearack {
    my($s, $src, $g_counter) = @_;

    my $queue = $s->{'connections'}{$src}{'responsequeue'};
    for(my $i = 0; $i < @{$queue}; ++$i) {
        my $cmd = $queue->[$i][0];
        if($cmd->g_counter == $g_counter) {
            splice($queue, $i, 1);
            last;
        }
    }
}

sub settimeoutcallback {
    my($s, $callback) = @_;

    $s->{'timeout_callback'} = $callback;
}

sub checkconns {
    my($s, $intv) = @_;

    foreach my $glob (keys(%{$s->{'connections'}})) {
        if($s->{'connections'}{$glob}{'persistent'}) {
            next;
        }

        my $counter = $s->{'connections'}{$glob}{'counter'} + 1;
        if($counter > 30) {
            if( $s->{'timeout_callback'} &&
                $s->{'timeout_callback'}($s, $glob)
            ) {
                $s->{'connections'}{$glob}{'counter'} = 0;
            } else {
                delete($s->{'connections'}{$glob});
            }
        } else {
            $s->{'connections'}{$glob}{'counter'} = $counter;
        }
    }
}

sub parsecmds {
    my($s, $gs) = @_;

    #If there are packets...
    while($s->{'s'}->recv(my $packet, 1450)) {
        my $ip = $s->{'s'}->peeraddr();
        my $port = $s->{'s'}->peerport();
        my $src = sockaddr_in($port, $ip);

        $s->_checkdest($src);
        $s->{'connections'}{$src}{'counter'} = 0;

        my @cmds = Gate88::Core::Util::CommandParser::parse($packet);
        foreach my $cmd (@cmds) {
            $s->_parsecmd($gs, $cmd, $src);
        }
    }
}

sub _parsecmd {
    my($s, $gs, $cmd, $src) = @_;

    my $type = $cmd->TYPE;
    my $important = $cmd->IMPORTANT;

    Gate88::Core::Logger::log("GET>" . ref($cmd) . ":IMPORTANT:$important", 5);
    Gate88::Core::Logger::log($cmd->dump(), 6);
    if($important && $s->{'autoack'}) {
        $s->queuecmd(Gate88::Core::Util::CommandBuilder::CommandAcknowledge($cmd), $src, 0);
    }

    if($cmd->TYPE == Gate88::Core::Command::CommandAcknowledge::TYPE) {
        $s->_clearack($src, $cmd->g_counter);
    }

    #Call callbacks
    my @c = exists($s->{'callbacks'}[$type]) ? @{$s->{'callbacks'}[$type]} : ();

    for(my $i = 0; $i < @c; ++$i) {
        $c[$i]($s, $cmd, $src, $gs);
    }
}
sub _procqueue {
    my($s, $target, $dest, $intv) = @_;
    my $queue = $s->{'connections'}{$dest}{$target};
    my $timeout = 1;

    my @packets;
    my @delayed;
    my @responsedelayed;
    my $packet = '';
    while(@{$queue}) {
        if(length($packet) + $queue->[0][0]->SIZE > 1450) {
            push(@packets, $packet);
            $packet = '';
            #Limit to 1 packet per tick. This runs every 0.1s so if we go over, something is probably wrong.
            last;
        }

        my $data = shift(@{$queue});
        $data->[1] -= $intv;

        if($data->[1] > 0) {
            push(@delayed, $data);
            next;
        }

        Gate88::Core::Logger::log("SEND>" . ref($data->[0]), 5);
        Gate88::Core::Logger::log($data->[0]->dump(), 6);
        $packet .= $data->[0]->pack();

        #Look for an Ack if it's important
        my $important = $data->[0]->IMPORTANT;
        if($important && $s->{'autoresend'}) {
            push(@responsedelayed, [$data->[0], $timeout]);
        }
    }
    if(length($packet)) {
        push(@packets, $packet);
    }
    $s->{'connections'}{$dest}{$target} = \@delayed;
    foreach my $data (@responsedelayed) {
        push(@{$s->{'connections'}{$dest}{'responsequeue'}}, $data);
    }

    foreach my $packet (@packets) {
        $s->{'s'}->send($packet, 0, $dest);
    }
}

sub flush {
    my($s, $intv) = @_;

    foreach my $dest (keys(%{$s->{'connections'}})) {
        my($port, $ip) = sockaddr_in($dest);
        Gate88::Core::Logger::log("DEST " . inet_ntoa($ip) . ":$port", 5);

        $s->_procqueue('responsequeue', $dest, $intv);
        $s->_procqueue('queue', $dest, $intv);
    }
}

sub _checkdest {
    my($s, $dest) = @_;

    if(!defined($s->{'connections'}{$dest})) {
        my($port, $ip) = sockaddr_in($dest);
        $s->{'connections'}{$dest} = Gate88::Core::Connection->new($ip, $port);
    }
}

sub _gendests {
    my($s, $exclude, $filter) = @_;

    my @ret;
    foreach my $dest (keys(%{$s->{'connections'}})) {
        my $type = $s->{'connections'}{$dest}->{'type'};
        if( !(grep{$_ eq $dest} @$exclude) &&
             (@$filter == 0 || grep{$_ == $type;} @$filter)
        ) {
            push(@ret, $dest);
        }
    }
    return @ret;
}

sub addconn {
    my($s, $key, $conn) = @_;

    $s->{'connections'}{$key} = $conn;
}

sub queuecmd {
    my($s, $cmd, $dest, $delay) = @_;

    $s->_checkdest($dest);
    push(@{$s->{'connections'}{$dest}{'queue'}}, [$cmd, $delay]);
}

sub sendcmd {
    my($s, $cmd, $dest) = @_;

    $s->_checkdest($dest);
    $s->{'s'}->send($cmd->pack(), 0, $dest);
}

sub queuecmdbroadcast {
    my($s, $cmd, $exclude, $filter, $delay) = @_;

    my @dests = $s->_gendests($exclude, $filter);
    foreach my $dest (@dests) {
        $s->queuecmd($cmd, $dest, $delay);
    }
}

sub sendcmdbroadcast {
    my($s, $cmd, $exclude, $filter) = @_;

    my @dests = $s->_gendests($exclude, $filter);
    foreach my $dest (@dests) {
        $s->sendcmd($cmd, $dest);
    }
}

sub counter {
    my($s, $type) = @_;

    my $count = defined($s->{'counters'}[$type]) ? $s->{'counters'}[$type] : 0;

    $count = ($count + 1) & 0xff;
    $s->{'counters'}[$type] = $count;

    return $count;
}

sub g_counter {
    my($s, $type) = @_;

    my $count = $s->{'g_counter'};

    $s->{'g_counter'} = ($count + 1) & 0xffff;

    return $count;
}

sub addcallback {
    my($s, $type, $func) = @_;
    my $c = $s->{'callbacks'};

    if(!defined($c->[$type])) {
        $c->[$type] = [];
    }

    push(@{$c->[$type]}, $func);
}

sub remcallback {
    my($s, $type, $func) = @_;
    my $c = $s->{'callbacks'};

    if(!defined($c->[$type])) {
        return;
    }

    my $t = $c->[$type];

    for(my $i = 0; $i < @$t; ++$i) {
        if($func == $t->[$i]) {
            splice(@$t, $i, 1);
            --$i;
        }
    }
}
1;
