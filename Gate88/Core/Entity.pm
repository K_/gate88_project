package Gate88::Core::Entity;
use parent Gate88::Core::Object;

use Modern::Perl;

use constant {
    FIELDS => ['x', 'y', 'x_velocity', 'y_velocity', 'hp', 'angle', 'cloak']
};

#Pull in all the Entities and initialize the type table
our @TABLE = Gate88::Core::Object::load_table('Entity');

sub new {
    my $class = shift;
    my $self = $class->SUPER::new(@_);

    $self->{'id'} = 0;
    $self->{'player_id'} = 0;

    return $self;
}

sub initialize {
    my($s, $cmd) = @_;

    $s->{'id'} = $cmd->entity_id;
    $s->{'player_id'} = $cmd->player_id;

    my @results = ($cmd->x, $cmd->y, 0, 0, $s->HP, 0);
    my @fields = @{$s->FIELDS};
    for(my $i = 0; $i < @fields; ++$i) {
        my $name = $fields[$i];
        $s->{'fields'}{$name} = $results[$i];
    }
}
1;