package Gate88::Core::Command::YardGroupChangeNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x87,
    SIZE => 17,
    P_SIZE => 12,
    IMPORTANT => 1,
    PACKSTR => 'SLLL',
    FIELDS => ['g_counter', 'player_id', 'yard_id', 'group']
};
1;