package Gate88::Core::Command::PlayerStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7c,
    SIZE => 22,
    P_SIZE => 19,
    IMPORTANT => 0,
    PACKSTR => 'LCLLLCC',
    FIELDS => ['player_id', 'upgrade', 'resources', 'max_resources', 'research_percent', 'style', 'level']
};
1;