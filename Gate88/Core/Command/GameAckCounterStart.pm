package Gate88::Core::Command::GameAckCounterStart;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x65,
    SIZE => 9,
    P_SIZE => 4,
    IMPORTANT => 1,
    PACKSTR => 'SL',
    FIELDS => ['g_counter', 'unknown']
};
1;