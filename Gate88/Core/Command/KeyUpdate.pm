package Gate88::Core::Command::KeyUpdate;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x66,
    SIZE => 5,
    P_SIZE => 2,
    IMPORTANT => 0,
    PACKSTR => 'CC',
    FIELDS => ['key', 'key_state']
};
1;