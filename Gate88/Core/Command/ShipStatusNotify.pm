package Gate88::Core::Command::ShipStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x79,
    SIZE => 39,
    P_SIZE => 36,
    IMPORTANT => 0,
    PACKSTR => 'LfffffffCCCC',
    FIELDS => ['entity_id', 'x', 'y', 'x_angle', 'y_angle', 'x_velocity', 'y_velocity', 'hp', 'special', 'keys', 'status', 'counter']
};
1;