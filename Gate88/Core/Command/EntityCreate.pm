package Gate88::Core::Command::EntityCreate;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x67,
    SIZE => 26,
    P_SIZE => 21,
    IMPORTANT => 1,
    PACKSTR => 'SLffffC',
    FIELDS => ['g_counter', 'entity_type', 'x', 'y', 'x_angle', 'y_angle', 'counter']
};
1;