package Gate88::Core::Command::KeysNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7a,
    SIZE => 9,
    P_SIZE => 6,
    IMPORTANT => 0,
    PACKSTR => 'LS',
    FIELDS => ['entity_id', 'keys']
};
1;