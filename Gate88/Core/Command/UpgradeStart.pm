package Gate88::Core::Command::UpgradeStart;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x68,
    SIZE => 9,
    P_SIZE => 4,
    IMPORTANT => 1,
    PACKSTR => 'SL',
    FIELDS => ['g_counter', 'upgrade']
};
1;