package Gate88::Core::Command::RoundTimeNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x8a,
    SIZE => 7,
    P_SIZE => 4,
    IMPORTANT => 0,
    PACKSTR => 'L',
    FIELDS => ['time']
};
1;