package Gate88::Core::Command::MessageSentNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x73,
    SIZE => 202,
    P_SIZE => 197,
    IMPORTANT => 1,
    PACKSTR => 'SLCa[192]',
    FIELDS => ['g_counter', 'player_id', 'unknown', 'message']
};
1;