package Gate88::Core::Command::PlayerKillsNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x8e,
    SIZE => 19,
    P_SIZE => 16,
    IMPORTANT => 0,
    PACKSTR => 'fffL',
    FIELDS => ['x', 'y', 'resources', 'player_id']
};
1;