package Gate88::Core::Command::ServerDisconnect;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6a,
    SIZE => 3,
    P_SIZE => 0,
    IMPORTANT => 0,
    PACKSTR => '',
    FIELDS => []
};
1;