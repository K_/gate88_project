package Gate88::Core::Command::RoundEndNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x89,
    SIZE => 5,
    P_SIZE => 0,
    IMPORTANT => 1,
    PACKSTR => 'S',
    FIELDS => ['g_counter']
};
1;