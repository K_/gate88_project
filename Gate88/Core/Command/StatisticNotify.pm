package Gate88::Core::Command::StatisticNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x91,
    SIZE => 20,
    P_SIZE => 17,
    IMPORTANT => 0,
    PACKSTR => 'LCfSSSS',
    FIELDS => ['player_id', 'counter', 'efficiency', 'statistic_type', 'unknown1', 'unknown2', 'unknown3']
};
1;