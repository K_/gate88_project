package Gate88::Core::Command::UNKNOWN130;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x82,
    SIZE => 35,
    P_SIZE => 32,
    IMPORTANT => 0,
    PACKSTR => 'LLffffff',
    FIELDS => ['target_id', 'entity_id', 'x', 'y', 'unknown1', 'unknown2', 'unknown3', 'unknown4']
};
1;