package Gate88::Core::Command::AllianceRequestNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x94,
    SIZE => 10,
    P_SIZE => 5,
    IMPORTANT => 1,
    PACKSTR => 'SLC',
    FIELDS => ['g_counter', 'player_id', 'counter']
};
1;