package Gate88::Core::Command::YardGroupChangeAck;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6c,
    SIZE => 9,
    P_SIZE => 4,
    IMPORTANT => 1,
    PACKSTR => 'SL',
    FIELDS => ['g_counter', 'group']
};
1;