package Gate88::Core::Command::ClientConnectionDeny;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x0a,
    SIZE => 4,
    P_SIZE => 1,
    IMPORTANT => 0,
    PACKSTR => 'C',
    FIELDS => ['reason']
};
1;