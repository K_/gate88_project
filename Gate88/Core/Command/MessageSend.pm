package Gate88::Core::Command::MessageSend;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6b,
    SIZE => 198,
    P_SIZE => 193,
    IMPORTANT => 1,
    PACKSTR => 'SCa[192]',
    FIELDS => ['g_counter', 'counter', 'message']
};
1;