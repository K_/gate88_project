package Gate88::Core::Command::EntityDelete;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x71,
    SIZE => 22,
    P_SIZE => 17,
    IMPORTANT => 1,
    PACKSTR => 'SCffff',
    FIELDS => ['g_counter', 'counter', 'x', 'y', 'x_angle', 'y_angle']
};
1;