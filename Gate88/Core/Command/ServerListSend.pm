package Gate88::Core::Command::ServerListSend;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x07,
    SIZE => 1210,
    P_SIZE => 1205,
    IMPORTANT => 1,
    PACKSTR => 'SCLa[1200]',
    FIELDS => ['g_counter', 'page', 'count', 'glob']
};
1;