package Gate88::Core::Command::ServerListRequest;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x05,
    SIZE => 6,
    P_SIZE => 1,
    IMPORTANT => 1,
    PACKSTR => 'SC',
    FIELDS => ['g_counter', 'page']
};
1;