package Gate88::Core::Command::ShipUpdate;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x72,
    SIZE => 27,
    P_SIZE => 24,
    IMPORTANT => 0,
    PACKSTR => 'ffffff',
    FIELDS => ['x_angle', 'y_angle', 'x', 'y', 'x_velocity', 'y_velocity']
};
1;