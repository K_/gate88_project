package Gate88::Core::Command::StatisticsRequest;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x74,
    SIZE => 10,
    P_SIZE => 5,
    IMPORTANT => 1,
    PACKSTR => 'SCL',
    FIELDS => ['g_counter', 'unknown', 'player_id']
};
1;