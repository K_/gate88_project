package Gate88::Core::Command::EntityStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x90,
    SIZE => 18,
    P_SIZE => 15,
    IMPORTANT => 0,
    PACKSTR => 'LffCCC',
    FIELDS => ['entity_id', 'x', 'y', 'hp', 'cloak', 'counter']
};
1;