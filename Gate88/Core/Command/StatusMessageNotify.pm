package Gate88::Core::Command::StatusMessageNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x8d,
    SIZE => 69,
    P_SIZE => 64,
    IMPORTANT => 1,
    PACKSTR => 'Sa[64]',
    FIELDS => ['g_counter', 'message']
};
1;