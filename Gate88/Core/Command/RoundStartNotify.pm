package Gate88::Core::Command::RoundStartNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x88,
    SIZE => 5,
    P_SIZE => 0,
    IMPORTANT => 1,
    PACKSTR => 'S',
    FIELDS => ['g_counter']
};
1;