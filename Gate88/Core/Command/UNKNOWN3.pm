package Gate88::Core::Command::UNKNOWN3;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x03,
    SIZE => 131, #UNKNOWN
    P_SIZE => 128, #UNKNOWN
    IMPORTANT => 0, #UNKNOWN
    PACKSTR => 'a[128]', #UNKNOWN
    FIELDS => ['status'] #UNKNOWN
};
1;
