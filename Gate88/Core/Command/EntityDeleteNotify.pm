package Gate88::Core::Command::EntityDeleteNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x83,
    SIZE => 9,
    P_SIZE => 4,
    IMPORTANT => 1,
    PACKSTR => 'SL',
    FIELDS => ['g_counter', 'entity_id']
};
1;