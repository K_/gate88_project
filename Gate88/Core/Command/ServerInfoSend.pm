package Gate88::Core::Command::ServerInfoSend;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x09,
    SIZE => 62,
    P_SIZE => 59,
    IMPORTANT => 0,
    PACKSTR => 'CCCLa[50]S',
    FIELDS => ['counter', 'players', 'max_players', 'time', 'name', 'version']
};
1;