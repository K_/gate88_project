package Gate88::Core::Command::TurretStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x8f,
    SIZE => 24,
    P_SIZE => 21,
    IMPORTANT => 0,
    PACKSTR => 'LLffCCCCC',
    FIELDS => ['entity_id', 'target_id', 'x', 'y', 'counter', 'angle', 'cooldown', 'hp', 'cloak']
};
1;