package Gate88::Core::Command::ShipStyleChange;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6d,
    SIZE => 6,
    P_SIZE => 1,
    IMPORTANT => 1,
    PACKSTR => 'SC',
    FIELDS => ['g_counter', 'style']
};
1;