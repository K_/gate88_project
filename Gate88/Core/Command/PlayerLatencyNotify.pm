package Gate88::Core::Command::PlayerLatencyNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x81,
    SIZE => 11,
    P_SIZE => 8,
    IMPORTANT => 0,
    PACKSTR => 'LL',
    FIELDS => ['player_id', 'ping']
};
1;