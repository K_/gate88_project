package Gate88::Core::Command::ServerInfoRequest;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6f,
    SIZE => 4,
    P_SIZE => 1,
    IMPORTANT => 0,
    PACKSTR => 'C',
    FIELDS => ['counter']
};
1;