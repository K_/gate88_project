package Gate88::Core::Command::FightersOrder;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x69,
    SIZE => 21,
    P_SIZE => 16,
    IMPORTANT => 1,
    PACKSTR => 'SLLff',
    FIELDS => ['g_counter', 'group', 'order', 'x', 'y']
};
1;