package Gate88::Core::Command::PlayerJoinNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x75,
    SIZE => 41,
    P_SIZE => 36,
    IMPORTANT => 1,
    PACKSTR => 'SLa[32]',
    FIELDS => ['g_counter', 'player_id', 'name']
};
1;