package Gate88::Core::Command::ServerRegistrationAcknowledge;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x06,
    SIZE => 6,
    P_SIZE => 1,
    IMPORTANT => 1,
    PACKSTR => 'SC',
    FIELDS => ['g_counter', 'counter']
};
1;