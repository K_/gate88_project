package Gate88::Core::Command::AllianceStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x95,
    SIZE => 11,
    P_SIZE => 6,
    IMPORTANT => 1,
    PACKSTR => 'SCLC',
    FIELDS => ['g_counter', 'counter', 'player_id', 'status']
};
1;