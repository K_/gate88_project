package Gate88::Core::Command::ServerConnect;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x08,
    SIZE => 45,
    P_SIZE => 40,
    IMPORTANT => 1,
    PACKSTR => 'SLLa[32]',
    FIELDS => ['g_counter', 'version', 'conn_speed', 'name']
};
1;