package Gate88::Core::Command::ServerRegistrationPing;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x0c,
    SIZE => 8,
    P_SIZE => 5,
    IMPORTANT => 0,
    PACKSTR => 'CSS',
    FIELDS => ['counter', 'unknown', 'port']
};
1;