package Gate88::Core::Command::FighterSpawnNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x86,
    SIZE => 21,
    P_SIZE => 16,
    IMPORTANT => 1,
    PACKSTR => 'SLLff',
    FIELDS => ['g_counter', 'yard_id', 'entity_id', 'x', 'y']
};
1;