package Gate88::Core::Command::FighterStatusNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x80,
    SIZE => 22,
    P_SIZE => 19,
    IMPORTANT => 0,
    PACKSTR => 'LffSCCCCC',
    FIELDS => ['entity_id', 'x', 'y', 'counter', 'angle', 'unknown1', 'unknown2', 'unknown3', 'hp']
};
1;