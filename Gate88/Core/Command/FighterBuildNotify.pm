package Gate88::Core::Command::FighterBuildNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7e,
    SIZE => 17,
    P_SIZE => 12,
    IMPORTANT => 1,
    PACKSTR => 'SLLL',
    FIELDS => ['g_counter', 'yard_id', 'player_id', 'entity_id']
};
1;