package Gate88::Core::Command::RoundEndTextSend;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x92,
    SIZE => 214,
    P_SIZE => 209,
    IMPORTANT => 1,
    PACKSTR => 'SCa[208]',
    FIELDS => ['g_counter', 'position', 'text']
};
1;