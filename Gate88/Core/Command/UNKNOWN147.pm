package Gate88::Core::Command::UNKNOWN147;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x93,
    SIZE => 15,
    P_SIZE => 10,
    IMPORTANT => 1,
    PACKSTR => 'SLLS',
    FIELDS => ['g_counter', 'unknown', 'unknown1', 'counter?']
};
1;