package Gate88::Core::Command::CommandAcknowledge;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x02,
    SIZE => 9,
    P_SIZE => 6,
    IMPORTANT => 0,
    PACKSTR => 'LS',
    FIELDS => ['type', 'g_counter']
};
1;