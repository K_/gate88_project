package Gate88::Core::Command::UpgradeCompleteNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7d,
    SIZE => 14,
    P_SIZE => 9,
    IMPORTANT => 1,
    PACKSTR => 'SLLC',
    FIELDS => ['g_counter', 'player_id', 'upgrade', 'level']
};
1;