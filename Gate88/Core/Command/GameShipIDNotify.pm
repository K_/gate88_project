package Gate88::Core::Command::GameShipIDNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x77,
    SIZE => 13,
    P_SIZE => 8,
    IMPORTANT => 1,
    PACKSTR => 'SLL',
    FIELDS => ['g_counter', 'entity_id', 'unknown1']
};
1;