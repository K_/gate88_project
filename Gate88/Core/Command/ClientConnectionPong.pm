package Gate88::Core::Command::ClientConnectionPong;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x01,
    SIZE => 5,
    P_SIZE => 2,
    IMPORTANT => 0,
    PACKSTR => 'S',
    FIELDS => ['counter']
};
1;