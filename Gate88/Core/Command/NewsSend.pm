package Gate88::Core::Command::NewsSend;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x0d,
    SIZE => 261,
    P_SIZE => 256,
    IMPORTANT => 1,
    PACKSTR => 'Sa[256]',
    FIELDS => ['g_counter', 'news']
};
1;