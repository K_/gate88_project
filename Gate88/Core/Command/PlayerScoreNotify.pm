package Gate88::Core::Command::PlayerScoreNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x8b,
    SIZE => 15,
    P_SIZE => 10,
    IMPORTANT => 1,
    PACKSTR => 'SLCCCCS',
    FIELDS => ['g_counter', 'player_id', 'kills', 'deaths', 'cc_kills', 'cc_deaths', 'score']
};
1;