package Gate88::Core::Command::FightersOrderNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7f,
    SIZE => 26,
    P_SIZE => 21,
    IMPORTANT => 1,
    PACKSTR => 'SCLLLff',
    FIELDS => ['g_counter', 'counter', 'player_id', 'group' , 'order', 'x', 'y']
};
1;