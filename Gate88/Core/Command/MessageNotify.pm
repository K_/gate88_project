package Gate88::Core::Command::MessageNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x85,
    SIZE => 203,
    P_SIZE => 198,
    IMPORTANT => 1,
    PACKSTR => 'SLCCa[192]',
    FIELDS => ['g_counter', 'player_id', 'counter', 'message_type', 'message']
};
1;