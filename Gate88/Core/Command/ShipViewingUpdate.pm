package Gate88::Core::Command::ShipViewingUpdate;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x6e,
    SIZE => 7,
    P_SIZE => 4,
    IMPORTANT => 0,
    PACKSTR => 'L',
    FIELDS => ['entity_id']
};
1;