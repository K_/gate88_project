package Gate88::Core::Command::EntityCreateNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x76,
    SIZE => 25,
    P_SIZE => 20,
    IMPORTANT => 1,
    PACKSTR => 'SLLLff',
    FIELDS => ['g_counter', 'entity_id', 'player_id', 'entity_type', 'x', 'y']
};
1;