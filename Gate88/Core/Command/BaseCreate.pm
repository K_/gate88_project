package Gate88::Core::Command::BaseCreate;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x70,
    SIZE => 21,
    P_SIZE => 16,
    IMPORTANT => 1,
    PACKSTR => 'Sffff',
    FIELDS => ['g_counter', 'x', 'y', 'x_angle', 'y_angle']
};
1;