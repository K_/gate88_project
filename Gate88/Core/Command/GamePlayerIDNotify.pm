package Gate88::Core::Command::GamePlayerIDNotify;
use parent qw/Gate88::Core::Command/;

use constant {
    TYPE => 0x7b,
    SIZE => 9,
    P_SIZE => 4,
    IMPORTANT => 1,
    PACKSTR => 'SL',
    FIELDS => ['g_counter', 'player_id']
};
1;