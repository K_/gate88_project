package Gate88::Core::Command;
use parent Gate88::Core::Object;

use Modern::Perl;

#Pull in all the Commands and initialize the type table
our @TABLE = Gate88::Core::Object::load_table('Command');

sub pack {
    my($s) = @_;

    my @input;
    my @fields = @{$s->FIELDS};
    for(my $i = 0; $i < @fields; ++$i) {
        my $name = $fields[$i];
        if(!defined($s->{'fields'}{$name})) {
            $s->d();
            warn("Value not defined: $name");
        }

        push(@input, $s->{'fields'}{$name});
    }

    return CORE::pack('(CS' . $s->PACKSTR . ')>', $s->TYPE, $s->P_SIZE, @input);
}

sub initialize {
    my($s, $data) = @_;

    my @results = unpack('(x[3]' . $s->PACKSTR . ')>', $data);

    {#DEBUG
        my $x = $s->P_SIZE;
        my $len = unpack('(xS)>', $data);
        if($len != $x) {
            warn("Incorrect p_size for " . ref($s) . " (Should be $len)\n");
        }

        my $expected = length(CORE::pack('(CS' . $s->PACKSTR . ')>', 0, 0, 0, 0, 0, 0, 0, 0));
        if(length($data) != $expected) {
            warn("Incorrect packstr for " . ref($s) . ", Got: " . length($data) . ", Expected: $expected\n");
        }
    }

    my @fields = @{$s->FIELDS};
    for(my $i = 0; $i < @fields; ++$i) {
        my $name = $fields[$i];
        $s->{'fields'}{$name} = $results[$i];
    }
}
1;