package Gate88::Core::Logger;

use constant {
    'LEVEL' => 4
};

sub log {
    my($str, $level) = @_;

    if(!defined($level)) {
        $level = 3;
    }
#    my $out = $level > 0 ? *STDERR :*STDOUT;
    my $out = *STDOUT;

    if(LEVEL >= $level) {
        print $out $str . "\n";
    }
}
1;