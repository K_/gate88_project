package Gate88::Core::Upgrade;

use Modern::Perl;

use constant {
    'BATTERY' => 0,
    'MECHANICS' => 1,
    'HEAVYMATERIALS' => 2,
    'WEAPONPOD' => 3,
    'SIGNALS' => 4,
    'MINITURRET' => 5,
    'REGENCONFIG' => 6,
    'HEAVYCONFIG' => 7,
    'NORMALCONFIG' => 8,
    'STEALTHCONFIG' => 9,
    'NONE' => 255
};

our %UPGRADES = (
    0 => _upgrade('Battery',  40, 3),
    1 => _upgrade('Mechanics', 40, 3),
    2 => _upgrade('Heavy Materials', 40, 3),
    3 => _upgrade('Weapon Pod', 100, 4),
    4 => _upgrade('Signal', 25, 5),
    5 => _upgrade('Mini Turret', 100, 3),
    6 => _upgrade('Regen Config', 200, 1),
    7 => _upgrade('Heavy Config', 250, 1),
    8 => _upgrade('Normal Config', 0, 1),
    9 => _upgrade('Stealth Config', 175, 1),
    255 => _upgrade('Nothing', 0, 0)
);

sub _upgrade {
    my($name, $cost, $max) = @_;
    return {'name'=>$name, 'cost'=>$cost, 'max'=>$max};
}

1;
