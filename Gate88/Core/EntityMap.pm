package Gate88::Core::EntityMap;

use v5.10;
use Modern::Perl;

use Math::Trig;
use Gate88::Core::Util::Function;
use POSIX qw/ceil/;

use constant {
    GRANULARITY => 0.5
};

sub new {
    my $class = shift;

    my $self = bless({}, $class);
    $self->reset();
    return $self;
}

sub reset {
    my($s) = @_;

    #Keep all ships
    my $ships = $s->{'type_map'}[Gate88::Core::Entity::PlayerShip::TYPE];
    my @tm = map { [] } @Gate88::Core::Entity::TABLE;

    $s->{'id_map'} = {};
    $s->{'spatial_map'} = {};
    $s->{'type_map'} = \@tm;

    for(my $i = 0; $ships && $i < @$ships; ++$i) {
        $s->add($ships->[$i]);
    }
}

sub add {
    my($s, $entity) = @_;

    my $id = $entity->{'id'};
    #TODO: Fixme, bombs are weird
    if($entity->TYPE == Gate88::Core::Entity::Bomb::TYPE) {
        return;
    }

    if(!defined($s->{'id_map'}{$id})) {
        $s->_addspatialent($entity);
        $s->{'id_map'}{$id} = $entity;
        push(@{$s->{'type_map'}[$entity->TYPE]}, $entity);
    }
}

sub rem {
    my($s, $id) = @_;

    if(defined($s->{'id_map'}{$id})) {
        my $entity = $s->{'id_map'}{$id};

        if($entity->TYPE == Gate88::Core::Entity::PlayerShip::TYPE) {
            return;
        }

        $s->_remtypeent($entity);
        $s->_remspatialent($entity);
        delete $s->{'id_map'}{$id};
    }
}

sub remgroup {

}

sub parseshipstatus {
    my($s, $cmd, $ent) = @_;

    $ent->x_velocity($cmd->x_velocity);
    $ent->y_velocity($cmd->y_velocity);
    $ent->angle(atan2($cmd->y_angle, $cmd->x_angle));
    $ent->hp($cmd->hp);
    #FIXME: Add special + status (Cloak, etc)
}

sub parsebuildingstatus {
    my($s, $cmd, $ent) = @_;

    #$ent->hp($cmd->hp * 1 / 85.0);
    $ent->cloak($cmd->cloak);
    #FIXME: Add cloak. Fix hp
}

sub parseturretstatus {
    my($s, $cmd, $ent) = @_;

    my $angle = $cmd->angle/127.5;
    $ent->angle($angle >= pi ? 2 * pi - $angle : $angle);
    #$ent->hp($cmd->hp * 1 / 85.0);
    $ent->cloak($cmd->cloak);
    $ent->cooldown($cmd->cooldown);
    #$ent->target($cmd->target);
    #$cmd->d();
    #FIXME: Add cloak, cooldown, target. Fix hp
}

sub parsefighterstatus {
    my($s, $cmd, $ent) = @_;

    my $angle = $cmd->angle/127.5;
    $ent->angle($angle >= pi ? 2 * pi - $angle : $angle);
    #$ent->hp($cmd->hp * 1 / 85.0);
    $ent->cloak($cmd->cloak);
    $ent->cooldown($cmd->cooldown);
    #$ent->target($cmd->target);
    #$cmd->d();
    #FIXME: Add cloak, cooldown, target. Fix hp
    $ent->d();
}

sub parseupdate {
    my($s, $cmd) = @_;

    my $entity = $s->get($cmd->entity_id);
    if(!$entity) {
        return;
    }

    $s->_update($entity, $cmd->x, $cmd->y);

    given($cmd->TYPE) {
        when(Gate88::Core::Command::ShipStatusNotify::TYPE) {
            $s->parseshipstatus($cmd, $entity);
        }
        when(Gate88::Core::Command::EntityStatusNotify::TYPE) {
            $s->parsebuildingstatus($cmd, $entity);
        }
        when(Gate88::Core::Command::TurretStatusNotify::TYPE) {
            $s->parseturretstatus($cmd, $entity);
        }
        when(Gate88::Core::Command::FighterStatusNotify::TYPE) {
            $s->parsefighterstatus($cmd, $entity);
        }
    }
}

sub parseleave {
    my($s, $cmd) = @_;

    my $id = $cmd->player_id;
    foreach my $k (keys(%{$s->{'id_map'}})) {
        if($id == $s->{'id_map'}{$k}->{'id'}) {
            $s->rem($k);
        }
    }
}

sub get {
    my($s, $id) = @_;

    return defined($s->{'id_map'}{$id}) ? $s->{'id_map'}{$id} : undef;
}

sub getnearby {
    my($s, $entity, $dist, $precise) = @_;

    my @ret;
    my @keys = $s->_nearbykeys($entity->x, $entity->y, $dist);

    foreach my $key (@keys) {
        my $map = $s->{'spatial_map'}{$key};
        if($map) {
            push(@ret, @$map);
        }
    }

    return @ret;
}

sub find {
    my($s, $types, $players, $dist, $hp) = @_;

    #FIXME: NOT DONE
}

sub plotpath {
    my($s, $entity, $dist, $precise) = @_;

    #FIXME: NOT DONE
    #A*!!!!
}

sub _update {
    my($s, $entity, $x, $y) = @_;

    my $update = $s->_genkey($x, $y) ne $s->_genkey($entity->x, $entity->y);

    if($update) {
        $s->_remspatialent($entity);
    }

    $entity->x($x);
    $entity->y($y);

    if($update) {
        $s->_addspatialent($entity);
    }
}

sub _nearbykeys {
    my($s, $x, $y, $dist) = @_;
    my @ret;

    my $d_sq = $dist**2;
    my $sz = int($dist/GRANULARITY) * GRANULARITY;

    for(my $i = -$sz; $i <= $sz; $i += GRANULARITY) {
        for(my $j = -$sz; $j <= $sz; $j += GRANULARITY) {
            if(($i)**2 + ($j)**2 > $d_sq) { next; }

            push(@ret, $s->_genkey($x + $i, $y + $j));
        }
    }

    return @ret;
}

sub _genkey {
    my($s, $x, $y) = @_;

    $x = clamp(-2**31, $x/GRANULARITY, 2**31-1);
    $y = clamp(-2**31, $y/GRANULARITY, 2**31-1);
    return pack('LL', $x, $y);
}

sub _addspatialent {
    my($s, $entity) = @_;
    my $key = $s->_genkey($entity->x, $entity->y);

    if(!defined($s->{'spatial_map'}{$key})) {
        $s->{'spatial_map'}{$key} = [];
    }

    push(@{$s->{'spatial_map'}{$key}}, $entity);
}

sub _remspatialent {
    my($s, $entity) = @_;
    my $key = $s->_genkey($entity->x, $entity->y);
    my $map = $s->{'spatial_map'}{$key};

    if(!$map) {
        $entity->d();
        die("NO MAP\n");
    }

    my $deleted = 0;
    #FIXME: Bad idea...
    for(my $i = 0; $i < @$map; ++$i) {
        if($map->[$i] == $entity) {
            splice($map, $i, 1);
            $deleted = 1;
            if(!@$map) {
                delete $s->{'spatial_map'}{$key};
            }
            last;
        }
    }

    if(!$deleted) {
        print $entity->{'id'} . "\n";
        $entity->d();
        die("NOT IN MAP\n");
    }
}

sub debug_find_entity {
    my($s, $id) = @_;

print "ENTS {\n";
    foreach my $k (keys(%{$s->{'spatial_map'}})) {
        my $map = $s->{'spatial_map'}{$k};
        for(my $i = 0; $i < @$map; ++$i) {
            print " > " . $map->[$i]->{'id'} . "\n";
            if($map->[$i]->{'id'} == $id) {
                print unpack('H*', $k) . " debug_find_ent found\n";
                return;
            }
        }
    }
    print " NOTFOUND\n";
}

sub _remtypeent {
    my($s, $entity) = @_;

    my $map = $s->{'type_map'}[$entity->TYPE];

    for(my $i = 0; $i < @$map; ++$i) {
        if($map->[$i] == $entity) {
            splice($map, $i);
            last;
        }
    }
}
1;
