package Gate88::Core::Object;

use Modern::Perl;

use File::Spec::Functions qw/catfile splitdir/;

our $AUTOLOAD;

#Pull in all the * and initialize the type table
sub load_table {
    my($type) = @_;
    my @table;

    my $command_dir = catfile('Gate88', 'Core', $type, '*.pm');
    foreach my $file (glob($command_dir)) {
        require $file;

        my $module = (split(/\./, join('::', splitdir($file))))[0];
        my $type = $module->TYPE;

        $table[$type] = $module;
    }
    return @table;
}

#Accessors for the command
sub AUTOLOAD {
    my($s, $v) = @_;

    my $str_mode = 0;
    my $field = (split('::', $AUTOLOAD))[-1];
    if(substr($field, 0, 1) eq '_') {
        $field = substr($field, 1);
        $str_mode = 1;
    }

    if(!exists($s->{'fields'}{$field})) {
        warn "Field $field doesn't exist for " . ref($s) . "\n";
        return undef;
    }

    my $val = $s->{'fields'}{$field};
    if($str_mode) {
        $val = (split("\0", $val))[0];
    }

    if(defined($v)) {
        $s->{'fields'}{$field} = $v;
    }

    return $val;
}

sub new {
    my $class = shift;
    my(@values) = @_;

    my @fields = @{$class->FIELDS};
    my $data = {};
    for(my $i = 0; $i < @fields; ++$i) {
        my $x =@values > $i ? $values[$i] : undef;
        $data->{$fields[$i]} = $x;
    }

    return bless({
        'fields' => $data,
    }, $class);
}

sub dump {
    my($s) = @_;
    my $ret = '';

    $ret .= ref($s) . "\n";
    foreach(keys %{$s->{'fields'}}) {
        $ret .= "  " . $_ . ' > ' . (defined($s->$_) ? $s->$_ : "UNDEF") . "\n";
    }
    return $ret;
}

sub d {
    my($s) = @_;
    print $s->dump();
}

sub DESTROY {}
1;