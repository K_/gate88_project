package Gate88::Core::Key;

use Modern::Perl;

use constant {
    'LEFT' => 0,
    'RIGHT' => 1,
    'UP' => 2,
    'DOWN' => 3,
    'SHOOT' => 4,
    'SPECIAL' => 5,

    'STATE_DOWN' => 0,
    'STATE_UP' => 1,
    'STATE_DOUBLE' => 2
};

our @KEYMAP = (
    [UP, STATE_DOWN],
    [DOWN, STATE_DOUBLE],
    [LEFT, STATE_DOWN],
    [RIGHT, STATE_DOWN],
    [SHOOT, STATE_DOWN],
    [DOWN, STATE_DOWN],
    [LEFT, STATE_DOUBLE],
    [RIGHT, STATE_DOUBLE]
);

1;