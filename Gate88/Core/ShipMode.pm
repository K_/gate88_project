package Gate88::Core::ShipMode;

use Modern::Perl;

use constant {
    'REGEN' => 0,
    'HEAVY' => 1,
    'STEALTH' => 2,
    'NORMAL' => 3
};

1;