package Gate88::MasterServer;
use parent qw/Gate88::Base/;

use Modern::Perl;

use IO::Socket::INET;

sub new {
    my $class = shift;

    my $s = IO::Socket::INET->new(LocalPort => 29981, Proto => 'udp', Blocking => 0) or die($!);
    my $self = $class->SUPER::new($s);

    $self->{'servers'} = {};

    $self->registercallbacks();

    return $self;
}

sub registercallbacks {
    my($s) = @_;

    $s->addcmdcallback(Gate88::Core::Command::ServerListRequest::TYPE, \&serverlist_req);
    $s->addcmdcallback(Gate88::Core::Command::ServerRegistrationPing::TYPE, \&server_reg);

    $s->settimeoutcallback(\&timeout);

    $s->addcommand('q', \&command_quit)
}

sub command_quit {
    my($s) = @_;

    $s->{'running'} = 0;
}

sub timeout {
    my($s, $src) = @_;

    my($port, $ip) = sockaddr_in($src);
    if(defined($s->{'servers'}{$src})) {
        delete($s->{'servers'}{$src});
        Gate88::Core::Logger::log("Server timeout: " . inet_ntoa($ip) . ":" . $port, 0);
    }
}

sub serverlist_req {
    my($s, $cmd, $src) = @_;
    my $page = $cmd->page;
    my $g_counter = $s->{'nm'}->g_counter();

    $s->{'connections'}{$src}->{'type'} = Gate88::Core::Connection::CLIENT;

    Gate88::Core::Logger::log("Server list requested: " . $page, 0);

    $s->{'nm'}->sendcmd(Gate88::Core::Util::CommandBuilder::ServerListSend($g_counter, $s->{'servers'}, $page), $src);

    if($page == 0) {
        $s->{'nm'}->sendcmd(Gate88::Core::Util::CommandBuilder::NewsSend(
            $g_counter, 'Contact: z@kwi.li'
        ), $src);
    }
}

sub server_reg {
    my($s, $cmd, $src) = @_;

    my($port, $ip) = sockaddr_in($src);
    my $counter = ($cmd->counter + 1) & 0xff;
    my $g_counter = $s->{'nm'}->g_counter();

    $s->{'connections'}{$src}->{'type'} = Gate88::Core::Connection::SERVER;

    if(defined($s->{'servers'}{$src})) {
        $s->{'nm'}->sendcmd(Gate88::Core::Util::CommandBuilder::ServerRegistrationPong($counter), $src);
    } else {
        Gate88::Core::Logger::log("Server registering: " . inet_ntoa($ip) . ":" . $port, 0);

        $s->{'nm'}->sendcmd(Gate88::Core::Util::CommandBuilder::ServerRegistrationAcknowledge($g_counter, $counter), $src);
    }
    $s->{'servers'}{$src} = 1;
}

1;