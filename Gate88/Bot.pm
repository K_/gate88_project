package Gate88::Bot;
use parent qw/Gate88::Base/;

use Modern::Perl;

use IO::Socket::INET;

sub new {
    my $class = shift;
    my($name) = @_;

    my $s = IO::Socket::INET->new(Proto => 'udp', PeerHost => 'localhost', PeerPort => 29987, Blocking => 0) or die($!);
    my $self = $class->SUPER::new($s);

    $self->{'gs'} = Gate88::Core::GameState->new($name);

    $self->registercallbacks();

    my $sin = sockaddr_in($s->peerport, $s->peeraddr);
    my $conn = Gate88::Core::Connection->new($s->peeraddr, $s->peerport);
    $conn->{'type'} = Gate88::Core::Connection::SERVER;
    $self->{'nm'}->addconn($sin, $conn);
    $self->{'server'} = $sin;

    #Join
    $self->connect($name);

    return $self;
}

sub registercallbacks {
    my($s) = @_;

    #Chat messages
    $s->addcmdcallback(Gate88::Core::Command::MessageNotify::TYPE, \&message_n);

    #Player packets
    $s->addcmdcallback(Gate88::Core::Command::PlayerJoinNotify::TYPE, \&playerjoin_n);
    $s->addcmdcallback(Gate88::Core::Command::PlayerStatusNotify::TYPE, \&playerstatus_n);
    $s->addcmdcallback(Gate88::Core::Command::PlayerScoreNotify::TYPE, \&playerscore_n);
    $s->addcmdcallback(Gate88::Core::Command::PlayerLeaveNotify::TYPE, \&playerleave_n);
    $s->addcmdcallback(Gate88::Core::Command::UpgradeCompleteNotify::TYPE, \&upgradecomplete_n);

    #Self packets
    $s->addcmdcallback(Gate88::Core::Command::GameLoadNotify::TYPE, \&gameload_n);
    $s->addcmdcallback(Gate88::Core::Command::GamePlayerIDNotify::TYPE, \&gameplayerid_n);
    $s->addcmdcallback(Gate88::Core::Command::GameShipIDNotify::TYPE, \&gameshipid_n);

    #GameState packets
    $s->addcmdcallback(Gate88::Core::Command::RoundStartNotify::TYPE, \&roundstart_n);
    $s->addcmdcallback(Gate88::Core::Command::RoundEndNotify::TYPE, \&roundend_n);
    $s->addcmdcallback(Gate88::Core::Command::RoundTimeNotify::TYPE, \&time_n);

    #Connection packets
    $s->addcmdcallback(Gate88::Core::Command::ClientConnectionPing::TYPE, \&clientconn_p);
    $s->addcmdcallback(Gate88::Core::Command::ClientConnectionDeny::TYPE, \&clientconn_d);

    #Entity packets
    $s->addcmdcallback(Gate88::Core::Command::EntityCreateNotify::TYPE, \&entcreate_n);
    $s->addcmdcallback(Gate88::Core::Command::EntityDeleteNotify::TYPE, \&entdelete_n);

    $s->addcmdcallback(Gate88::Core::Command::ShipStatusNotify::TYPE, \&shipstatus_n);
    $s->addcmdcallback(Gate88::Core::Command::EntityStatusNotify::TYPE, \&entstatus_n);
    $s->addcmdcallback(Gate88::Core::Command::TurretStatusNotify::TYPE, \&entstatus_n);
    $s->addcmdcallback(Gate88::Core::Command::FighterStatusNotify::TYPE, \&entstatus_n);

    #Alliance packets
    $s->addcmdcallback(Gate88::Core::Command::AllianceStatusNotify::TYPE, \&allystatus_n);

    #Tests for unknown packets
    $s->addcmdcallback(Gate88::Core::Command::StatisticNotify::TYPE, \&unknown145);
#    $s->addcmdcallback(Gate88::Core::Command::UNKNOWN130::TYPE, \&udump);
    $s->addcmdcallback(Gate88::Core::Command::UNKNOWN147::TYPE, \&udump);

    $s->settimeoutcallback(\&timeout);

    $s->addcommand('e', \&command_eval);
    $s->addcommand('t', \&command_talk);
    $s->addcommand('q', \&command_quit);
}

sub connect {
    my($s, $name) = @_;

    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::ServerConnect(
        $s->{'nm'}->counter(Gate88::Core::Command::ServerConnect::TYPE),
    $name), 0);
}

sub timeout {
    my($s) = @_;

    Gate88::Core::Logger::log('Timeout! Reconnecting', 0);
    $s->connect($s->{'gs'}{'self'}{'name'});

    #Reset timer
    return 1;
}

sub shutdown {
    my($s) = @_;

    $s->{'running'} = 0;
    $s->sendcmdserver(Gate88::Core::Util::CommandBuilder::ServerDisconnect());
}

sub command_quit {
    my($s) = @_;

    $s->shutdown();
}

sub command_talk {
    my($s, $data) = @_;

    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::MessageSend(
        $s->{'nm'}->g_counter(), $s->{'nm'}->counter(Gate88::Core::Command::MessageSend::TYPE), $data
    ), 0);
}

sub command_eval {
    my($s, $data) = @_;

    eval($data);
}


sub gameload_n {
    my($s, $cmd, $src) = @_;

    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::GameAckCounterStart($s->{'nm'}->g_counter()), 0);
    Gate88::Core::Logger::log("Game initialized", 2);
}

sub gameplayerid_n {
    my($s, $cmd, $src) = @_;

    $s->{'gs'}{'self'}{'id'} = $cmd->player_id;
    $s->{'gs'}{'players'}{$cmd->player_id} = $s->{'gs'}{'self'};
    Gate88::Core::Logger::log("Game player ID set: " . $cmd->player_id, 2);
}

sub gameshipid_n {
    my($s, $cmd, $src) = @_;

    my $entity = $s->{'gs'}{'entities'}->get($cmd->entity_id);
    $s->{'gs'}{'self'}{'ship'} = $entity;

    Gate88::Core::Logger::log("Game Ship ID set: " . $entity->{'id'}, 2);
}

sub message_n {
    my($s, $cmd, $src) = @_;

    my $name = $s->getplayername($cmd->player_id);
    my $msg = $cmd->_message;
    Gate88::Core::Logger::log("$name: $msg", 0);
}


sub playerjoin_n {
    my($s, $cmd, $src) = @_;
    my $player = Gate88::Core::Player->new();
    $player->initialize($cmd);
    $s->{'gs'}{'players'}{$player->{'id'}} = $player;

    Gate88::Core::Logger::log("Player joined: " . $player->{'name'} . ", ID:" . $cmd->player_id, 0);
}

sub playerstatus_n {
    my($s, $cmd, $src) = @_;
    my $player = $s->getplayer($cmd->player_id);
    if(!$player) { return; }

    $player->parsestatus($cmd);
}

sub playerscore_n {
    my($s, $cmd, $src) = @_;
    my $player = $s->getplayer($cmd->player_id);
    if(!$player) { return; }

    $player->parsescore($cmd);
}

sub playerleave_n {
    my($s, $cmd, $src) = @_;

    my $player = $s->getplayer($cmd->player_id);
    if($player) {
        delete($s->{'gs'}{'players'}{$cmd->player_id});
        $s->{'gs'}{'entities'}->parseleave($cmd);
        Gate88::Core::Logger::log("Player left: " . $player->{'name'} , 0);
    }
}

sub upgradecomplete_n {
    my($s, $cmd, $src) = @_;
    my $player = $s->getplayer($cmd->player_id);
    if(!$player) { return; }

    $player->parseupgrade($cmd);
}


sub roundstart_n {
    my($s, $cmd, $src) = @_;

    $s->{'gs'}->parsestart($cmd);
    Gate88::Core::Logger::log("Round start", 0);
}

sub roundend_n {
    my($s, $cmd, $src) = @_;

    $s->{'gs'}->parseend($cmd);
    Gate88::Core::Logger::log("Round end", 0);
}

sub time_n {
    my($s, $cmd, $src) = @_;

    $s->{'gs'}->parsetime($cmd);
}


sub clientconn_p {
    my($s, $cmd, $src) = @_;

    $s->queuecmdserver(Gate88::Core::Util::CommandBuilder::ClientConnectionPong($cmd->counter), 0);
}

sub clientconn_d {
    my($s, $cmd, $src) = @_;

    $s->{'running'} = 0;
    Gate88::Core::Logger::log("Connection rejected", 0);
}


sub entcreate_n {
    my($s, $cmd, $src) = @_;

    my $player = $s->getplayer($cmd->player_id);
    my $name = $player->{'name'};
    my $entity = Gate88::Core::Util::EntityParser::parse($cmd);

    if($entity) {
        my $type = $entity->NAME;
        my $id = $entity->{'id'};

        my $x = $entity->x;
        my $y = $entity->y;

        $s->{'gs'}{'entities'}->add($entity);

        if($entity->TYPE == Gate88::Core::Entity::CommandPost::TYPE) {
            $player->{'base'} = $entity;
        }

        Gate88::Core::Logger::log("Spawned: $type, ID: $id, Pos: ($x, $y), Player: $name", 1);
    }
}

sub entdelete_n {
    my($s, $cmd, $src) = @_;

    my $id = $cmd->entity_id;
    my $entity = $s->{'gs'}{'entities'}->get($id);
    my $player = $s->getplayer($entity->{'player_id'});
    $s->{'gs'}{'entities'}->rem($id);

    if($entity->TYPE == Gate88::Core::Entity::CommandPost::TYPE) {
        $player->{'base'} = undef;
    }

    Gate88::Core::Logger::log("Destroyed: ID: $id", 1);
}

sub entstatus_n {
    my($s, $cmd, $src) = @_;

    $s->{'gs'}{'entities'}->parseupdate($cmd);
}

sub shipstatus_n {
    my($s, $cmd, $src) = @_;

    #Reset all keys to off
    foreach my $i (0..5) {
        $s->{'gs'}{'self'}{'keys'}[$i] = Gate88::Core::Key::STATE_UP;
    }

    #Read in new data
    for(my $i = 0; $i < @Gate88::Core::Key::KEYMAP; ++$i) {
        my $bit = !!vec(chr($cmd->keys), $i, 1);

        if($bit) {
            my $key = $Gate88::Core::Key::KEYMAP[$i][0];
            my $state = $Gate88::Core::Key::KEYMAP[$i][1];
            $s->{'gs'}{'self'}{'keys'}[$key] = $state;
        }
    }

    $s->{'gs'}{'entities'}->parseupdate($cmd);
}

sub allystatus_n {
    my($s, $cmd, $src) = @_;
    my $player = $s->getplayer($cmd->player_id);

    $player->parseally($cmd);
}

#TODO: FighterOrder
#TODO: FighterSpawnNotify
#TODO: Keys*
#TODO: Message*?
#TODO: ShipStyleChange


#Debug functions
sub unknown {
    my($s, $cmd) = @_;

    my $type = $cmd->TYPE;
    Gate88::Core::Logger::log("UNKNOWN packet received: $type", 0);
}

sub unknown145 {
    my($s, $cmd) = @_;

    if($cmd->efficiency != 0 || $cmd->unknown1 != 0 || $cmd->unknown2 != 0) {
#        Gate88::Core::Logger::log("Detected change in 145!" . $cmd->dump(), 0);
    }
}

sub udump {
    my($s, $cmd) = @_;

    Gate88::Core::Logger::log($cmd->dump(), 0);
}

#Useful functions
sub sendcmdserver {
    my($s, $cmd, $delay) = @_;

    $s->{'nm'}->sendcmd($cmd, $s->{'server'});
}

sub queuecmdserver {
    my($s, $cmd, $delay) = @_;

    $s->{'nm'}->queuecmd($cmd, $s->{'server'}, $delay);
}

sub getplayer {
    my($s, $id) = @_;

    return defined($s->{'gs'}{'players'}{$id}) ? $s->{'gs'}{'players'}{$id} : undef;
}

sub getplayername {
    my($s, $id) = @_;
    my $name = '';

    if($id == 0) {
        $name = 'SERVER';
    } else {
        my $player = $s->getplayer($id);
        $name = $player ? $player->{'name'} : '???';
    }

    return $name;
}
1;
