package Gate88::Server;
use parent qw/Gate88::Base/;

use Modern::Perl;

use IO::Socket::INET;

sub new {
    my $class = shift;

    my $port = 29986;
    my $s = IO::Socket::INET->new(LocalPort => $port, Proto => 'udp', Blocking => 0) or die($!);
    my $self = $class->SUPER::new($s);
    $self->{'port'} = $port;

    $self->registercallbacks();

    my $host = gethostbyname('g88.kwi.li');
    my $sin = sockaddr_in(29981, $host);
    my $conn = Gate88::Core::Connection->new($s->peeraddr, $s->peerport);
    $conn->{'persistent'} = 1;
    $conn->{'type'} = Gate88::Core::Connection::MSERVER;
    $self->{'nm'}->addconn($sin, $conn);

    return $self;
}

sub registercallbacks {
    my($s) = @_;

    $s->addcmdcallback(Gate88::Core::Command::ServerConnect::TYPE, \&serverconn);
    $s->addcmdcallback(Gate88::Core::Command::GameAckCounterStart::TYPE, \&ackstart);
    $s->addcmdcallback(Gate88::Core::Command::ServerInfoRequest::TYPE, \&serverinfo_r);
    $s->addcmdcallback(Gate88::Core::Command::ServerRegistrationAcknowledge::TYPE, \&serverreg_a);

    $s->addintvcallback(0.1, \&update);
    $s->addintvcallback(1.0, \&registerserver);

    $s->addcommand('q', \&command_quit)
}

sub command_quit {
    my($s) = @_;

    $s->{'running'} = 0;
}

sub registerserver {
    my($s) = @_;

    $s->{'nm'}->queuecmdbroadcast(Gate88::Core::Util::CommandBuilder::ServerRegistrationPing(
        $s->{'nm'}->counter(Gate88::Core::Command::ServerRegistrationPing::TYPE),
        $s->{'port'}
    ), [], [Gate88::Core::Connection::MSERVER], 0);
}

sub serverreg_a {
    my($s, $cmd, $src) = @_;

    Gate88::Core::Logger::log('Registered with master server.', 0);
}

sub serverinfo_r {
    my($s, $cmd, $src) = @_;

    $s->{'nm'}->queuecmd(Gate88::Core::Util::CommandBuilder::ServerInfoSend(
        $cmd->counter,
        0, 0, 0, 'Fake Server'
    ), $src, 0);
}

sub serverconn {
    my($s, $cmd, $src) = @_;

    my $conn = $s->{'connections'}{$src};
    $conn->{'type'} = Gate88::Core::Connection::CLIENT;
    if($conn->{'status'}) {
        #FIXME: Handle this better
        #Map players to conns
        return;
    }
    $conn->{'status'} = 1;

    $s->queuecmd(Gate88::Core::Util::CommandBuilder::TimeNotify(1000 * 60), $src, 0);

    $s->queuecmd(Gate88::Core::Util::CommandBuilder::GamePlayerIDNotify(
        $s->{'nm'}->g_counter(), 9
    ), $src, 0);
    $s->queuecmd(Gate88::Core::Util::CommandBuilder::RoundStartNotify(
        $s->{'nm'}->g_counter()
    ), $src, 0);
    $s->queuecmd(Gate88::Core::Util::CommandBuilder::PlayerScoreNotify(
        $s->{'nm'}->g_counter(), 9, 0, 0, 0, 0, 0
    ), $src, 0);
    $s->queuecmd(Gate88::Core::Util::CommandBuilder::GameLoadNotify(
        $s->{'nm'}->g_counter()
    ), $src, 0);
}

sub ackstart {
    my($s, $cmd, $src) = @_;

    $s->queuecmd(Gate88::Core::Util::CommandBuilder::EntityCreateNotify(
        $s->{'nm'}->g_counter(), 1, 9, 0, 0, 0
    ), $src, 0);
    $s->queuecmd(Gate88::Core::Util::CommandBuilder::GameShipIDNotify(
        $s->{'nm'}->g_counter(), 1, 0
    ), $src, 0);
}

sub update {
    my($s) = @_;

}
1;