#!/usr/bin/perl
#Check if P_SIZE is really SIZE - HEADER - IMPORTANT * 2
use Modern::Perl;

use lib '..';

use Gate88::Core;

for(my $i = 0; $i < @Gate88::Core::Command::TABLE; ++$i) {
    if(defined($Gate88::Core::Command::TABLE[$i])) {
        my $module = $Gate88::Core::Command::TABLE[$i];

        my $size = $module->SIZE;
        my $p_size = $module->P_SIZE;
        my $important = $module->IMPORTANT;

        my $assert = $size == 3 + $important * 2 + $p_size;

        print(($assert ? 'PASS':'FAIL') . "   $module\n");
    }
}