#!/usr/bin/perl
#Check that all the packstrs are correct (They can deserialize)
use Modern::Perl;

use lib '..';

use Gate88::Core;

for(my $i = 0; $i < @Gate88::Core::Command::TABLE; ++$i) {
    if(defined($Gate88::Core::Command::TABLE[$i])) {
        my $module = $Gate88::Core::Command::TABLE[$i];

        my $obj = $module->new();
        eval {
            my $data = pack('(CS' . $obj->PACKSTR . ')>', $i, $obj->P_SIZE, 0, 0, 0, 0, 0, 0, 0, 0);
            $obj->initialize($data);
        };
        if ($@) {
            print ref($obj) . "'s packstr failed: $@\n";
        }
        #If there are any issues, we'll see an error
        my @data = unpack('(x[3]' . $obj->PACKSTR . ')>', "\0"x500);
        my $assert = length(@data) == length(@{$obj->FIELDS});

        if(!$assert) {
            print ref($obj) . " has an invalid length $@\n";
        }

        my $sz = length(join('', split(/[^a-zA-Z]/, $module->PACKSTR)));
        #Check if the packstr count == fields count
        if(@{$module->FIELDS} + 0 != $sz) {
            print "$module has a size mismatch\n";
        }
    }
}