#!/usr/bin/perl
use Modern::Perl;

use Gate88::Bot::Alpha;

my $bot;

$SIG{'INT'} = sub {
    $bot->shutdown();
    exit;
};

$bot = Gate88::Bot::Alpha->new();
$bot->run();