#!/usr/bin/perl
use Modern::Perl;

use Gate88::Bot::One;

my $bot;

$SIG{'INT'} = sub {
    $bot->shutdown();
    exit;
};

$bot = Gate88::Bot::One->new();
$bot->run();